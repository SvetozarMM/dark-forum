import React, { Fragment } from 'react'
import HeaderContainer from '../../components/main-container/header-container/header-container';
import BodyContainer from '../../components/main-container/body-container/body-container';
import SidebarComponent from '../../components/main-container/body-container/sidebar-container/sidebar-container';
import HeroContainer from '../../components/main-container/body-container/hero-container/hero-container';
import Logo from '../../components/UI/logo/logo';
import MenuNav from '../../components/UI/menu-nav/menu-nav';
import NavBtn from '../../components/UI/nav-btn/nav-btn';
import { Route, Redirect, withRouter } from 'react-router-dom';
import About from '../../containers/about/about';
import Terms from '../../containers/terms/terms';
import Auth from '../../containers/auth/auth';

const AuthPage = () => {
  return (
    <Fragment>
      <HeaderContainer>
        <Logo />
        <MenuNav>
          <NavBtn text='About Us' link={'/about-us'} />
          <NavBtn text='General Terms' link={'/general-terms'} />
          <NavBtn text='Login' link={'/login'} />
        </MenuNav>
      </HeaderContainer>

      <BodyContainer>
        <SidebarComponent></SidebarComponent>
        <HeroContainer>
          <Route path='/login' component={Auth} />
          <Route path='/about-us' component={About} />
          <Route path='/general-terms' component={Terms} />
          <Redirect from='/' to='/login' />
        </HeroContainer>
      </BodyContainer>
    </Fragment>
  );
}

export default withRouter(AuthPage);
