import React, { Fragment } from 'react';
import HeaderContainer from '../../components/main-container/header-container/header-container';
import BodyContainer from '../../components/main-container/body-container/body-container';
import SidebarComponent from '../../components/main-container/body-container/sidebar-container/sidebar-container';
import HeroContainer from '../../components/main-container/body-container/hero-container/hero-container';
import MenuNavHome from '../../components/UI/menu-nav-home/menu-nav-home';
import UserInfoBox from '../../components/UI/user-info-box/user-info-box';
import SearchBox from '../../components/UI/search-box/search-box';
import BtnAddPost from '../../components/UI/btn-add-post/btn-add-post';
import NavMenu from '../../components/nav-menu/nav-menu';
import { Route, Redirect, withRouter } from 'react-router-dom';
import Posts from '../../containers/posts/posts';
import About from '../../containers/about/about';
import Activity from '../../containers/activity/activity';
import Preferences from '../../containers/preferences/preferences';
import Controls from '../../containers/controls/controls';
import Logout from '../../containers/auth/logout';
import MyPosts from '../../containers/activity/my-posts/my-posts';
import MyComments from '../../containers/activity/my-comments/my-comments';
import LatestPosts from '../../components/latest-posts/latest-posts';

const MainPage = () => {
  return (
    <Fragment>
      <HeaderContainer>
        <UserInfoBox />

        <MenuNavHome>
          <SearchBox />
          <BtnAddPost />
          <NavMenu />
        </MenuNavHome>
      </HeaderContainer>

      <BodyContainer>
        <SidebarComponent>
          <LatestPosts />
        </SidebarComponent>
        <HeroContainer>
          <Route path='/posts' component={Posts} />
          <Route path='/activity' component={Activity} />
          <Route path='/my-posts' component={MyPosts} />
          <Route path='/my-comments' component={MyComments} />
          <Route path='/about' component={About} />
          <Route path='/preferences' component={Preferences} />
          <Route path='/controls' component={Controls} />
          <Route path='/logout' component={Logout} />
          <Redirect from='/' to='/posts' />
        </HeroContainer>
      </BodyContainer>
    </Fragment>
  );
};

export default withRouter(MainPage);
