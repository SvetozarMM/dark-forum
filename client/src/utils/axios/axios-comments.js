import axios from 'axios';
import * as constants from '../../constants/constants';

const instance = axios.create({
  baseURL: `${constants.API_DOMAIN_NAME}`,
});

export default instance;
