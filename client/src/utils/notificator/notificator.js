import { store } from 'react-notifications-component';

export const success = (msg) => {
  store.addNotification({
    message: msg,
    type: 'success',
    insert: 'top',
    container: 'top-center',
    animationIn: ['animated', 'fadeIn'],
    animationOut: ['animated', 'fadeOut'],
    dismiss: { duration: 2500 },
  });
};

export const error = (msg) => {
  store.addNotification({
    message: msg,
    type: 'danger',
    insert: 'top',
    container: 'top-center',
    animationIn: ['animated', 'fadeIn'],
    animationOut: ['animated', 'fadeOut'],
    dismiss: { duration: 2500 },
  });
};

export const info = (msg) => {
  store.addNotification({
    message: msg,
    type: 'info',
    insert: 'top',
    container: 'top-center',
    animationIn: ['animated', 'fadeIn'],
    animationOut: ['animated', 'fadeOut'],
    dismiss: { duration: 2500 },
  });
};

export const warning = (msg) => {
  store.addNotification({
    message: msg,
    type: 'warning',
    insert: 'top',
    container: 'top-center',
    animationIn: ['animated', 'fadeIn'],
    animationOut: ['animated', 'fadeOut'],
    dismiss: { duration: 2500 },
  });
};
