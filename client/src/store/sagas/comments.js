import { put } from 'redux-saga/effects';
import * as commentService from '../../services/comment-service';
import * as postsService from '../../services/posts-service';
import * as storageService from '../../services/storage-service';
import * as actions from '../actions/index';
import * as notificator from '../../utils/notificator/notificator';

export function* onFetchPostComments(action) {
  const {
    payload: { postId },
  } = action;

  try {
    const token = storageService.read('token');
    const res = yield postsService.getComments(postId, token);
    if (res.data) {
      yield put(actions.savePostComments(postId, res.data));
    }
  } catch (error) {
    yield notificator.error(`${error.response.data.error}`);
  }
}

export function* onFetchMyComments(action) {
  try {
    const token = storageService.read('token');
    const res = yield commentService.getAllOwnComments(token);
    if (res.data) {
      yield put(actions.saveMyComments(res.data));
    }
  } catch (error) {
    yield notificator.error(`${error.response.data.error}`);
  }
}

export function* onUpdateComment(action) {
  const { payload } = action;
  const { comment, postId } = payload;

  try {
    const token = storageService.read('token');
    const res = yield commentService.updateComment(comment, token);
    if (res.data) {
      yield put(actions.updateComment(res.data, postId));
      yield notificator.success(`Your comment has been successfully updated.`);
    }
  } catch (error) {
    yield notificator.error(`${error.response.data.error}`);
  }
}

export function* onLikeComment(action) {
  const { payload } = action;
  const { comment, postId } = payload;

  try {
    const token = storageService.read('token');
    const res = yield commentService.likeComment(comment, token);
    if (res.data) {
      yield put(actions.likeComment(res.data, postId));
    }
  } catch (error) {
    yield notificator.error(`${error.response.data.error}`);
  }
}

export function* onDeleteComment(action) {
  const { payload } = action;
  const { commentId, postId } = payload;

  try {
    const token = storageService.read('token');
    const res = yield commentService.deleteComment(commentId, token);
    if (res.data) {
      yield put(actions.deleteComment(commentId, postId));
      yield notificator.success(res.data.message);
    }
  } catch (error) {
    yield notificator.error(`${error.response.data.error}`);
  }
}

export function* onAddComment(action) {
  const { payload } = action;
  const { comment, postId } = payload;

  try {
    const token = storageService.read('token');
    const res = yield postsService.addComment(comment, postId, token);
    if (res.data) {
      yield put(actions.saveComment(res.data, postId));
      yield notificator.success(`Your comment has been successfully submitted.`);
    }
  } catch (error) {
    yield notificator.error(`${error.response.data.error}`);
  }
}
