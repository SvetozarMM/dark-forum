import { put } from 'redux-saga/effects';
import * as usersService from '../../services/users-service';
import * as storageService from '../../services/storage-service';
import * as actions from '../actions/index';
import * as notificator from '../../utils/notificator/notificator';

export function* onFetchUserActivity(action) {
  const {
    payload: { userId },
  } = action;

  try {
    const token = storageService.read('token');
    const res = yield usersService.getUserActivity(userId, token);
    if (res.data) {
      yield put(actions.saveUserActiviity(userId, res.data));
    }
  } catch (error) {
    yield notificator.error(`${error.response.data.error}`);
  }
}

export function* onFetchAllUsers(action) {
  try {
    const token = storageService.read('token');
    const res = yield usersService.getAllUsers(token);
    if (res.data) {
      yield put(actions.saveAllUsers(res.data));
    }
  } catch (error) {
    yield notificator.error(`${error.response.data.error}`);
  }
}

export function* onUpdateUserRole(action) {
  const {
    payload: { user },
  } = action;
  yield put(actions.saveUserRole(user));
  // try {
  //   const token = storageService.read('token');
  //   const res = yield usersService.updateUserRoles(action.user, token);
  //   if (res.data) {
  //     yield put(actions.saveUserRole(res.data));
  //   }
  // } catch (error) {
  //   yield notificator.error(`${error.response.data.error}`);
  // }
}

export function* onUpdateUserBanstatus(action) {
  const {
    payload: { user },
  } = action;

  try {
    const token = storageService.read('token');
    const res = yield usersService.updateUserBanstatus(user, token);
    if (res.data) {
      yield put(actions.saveUserBanstatus(res.data));
      const banstatus = res.data.banstatus.isBanned ? 'banned' : 'unbanned';
      yield notificator.warning(
        `User "${user.username}" has been ${banstatus}.`
      );
    }
  } catch (error) {
    yield notificator.error(`${error.response.data.error}`);
  }
}

export function* onDeleteUser(action) {
  const {
    payload: { user },
  } = action;

  try {
    const token = storageService.read('token');
    const res = yield usersService.deleteUser(user, token);
    if (res.data) {
      yield put(actions.deleteUser(user));
      yield notificator.success(
        `User "${user.username}" has been successfully deleted.`
      );
    }
  } catch (error) {
    yield notificator.error(`${error.response.data.error}`);
  }
}
