import { takeEvery, all } from 'redux-saga/effects';

import * as actionTypes from '../actions/actionTypes';
import {
  logoutSaga,
  checkAuthTimeoutSaga,
  registerUserSaga,
  authUserSaga,
  authCheckStateSaga,
} from './auth';

import {
  onFetchAllPosts,
  onFetchMyPosts,
  onUpdatePost,
  onLikePost,
  onToggleLockPost,
  onDeletePost,
  onAddPost,
} from './posts';

import {
  onFetchPostComments,
  onFetchMyComments,
  onUpdateComment,
  onLikeComment,
  onDeleteComment,
  onAddComment,
} from './comments';

import {
  onFetchUserActivity,
  onFetchAllUsers,
  onUpdateUserRole,
  onUpdateUserBanstatus,
  onDeleteUser,
} from './users';

export function* watchAuth() {
  yield all([
    yield takeEvery(actionTypes.AUTH_CHECK_TIMEOUT, checkAuthTimeoutSaga),
    yield takeEvery(actionTypes.AUTH_INITIATE_LOGOUT, logoutSaga),
    yield takeEvery(actionTypes.AUTH_REGISTER, registerUserSaga),
    yield takeEvery(actionTypes.AUTH_USER, authUserSaga),
    yield takeEvery(actionTypes.AUTH_CHECK_STATE, authCheckStateSaga),
  ]);
}

export function* watchPosts() {
  yield takeEvery(actionTypes.FETCH_ALL_POSTS, onFetchAllPosts);
  yield takeEvery(actionTypes.FETCH_MY_POSTS, onFetchMyPosts);
  yield takeEvery(actionTypes.UPDATE_POST_SAGA, onUpdatePost);
  yield takeEvery(actionTypes.LIKE_POST_SAGA, onLikePost);
  yield takeEvery(actionTypes.TOGGLE_LOCK_POST, onToggleLockPost);
  yield takeEvery(actionTypes.DELETE_POST_SAGA, onDeletePost);
  yield takeEvery(actionTypes.ADD_POST, onAddPost);
}

export function* watchComments() {
  yield takeEvery(actionTypes.FETCH_POST_COMMENTS, onFetchPostComments);
  yield takeEvery(actionTypes.FETCH_MY_COMMENTS, onFetchMyComments);
  yield takeEvery(actionTypes.UPDATE_COMMENT_SAGA, onUpdateComment);
  yield takeEvery(actionTypes.LIKE_COMMENT_SAGA, onLikeComment);
  yield takeEvery(actionTypes.DELETE_COMMENT_SAGA, onDeleteComment);
  yield takeEvery(actionTypes.ADD_COMMENT, onAddComment);
}

export function* watchUsers() {
  yield takeEvery(actionTypes.FETCH_USER_ACTIVITY, onFetchUserActivity);
  yield takeEvery(actionTypes.FETCH_ALL_USERS, onFetchAllUsers);
  yield takeEvery(actionTypes.UPDATE_USER_ROLE, onUpdateUserRole);
  yield takeEvery(actionTypes.UPDATE_USER_BANSTATUS, onUpdateUserBanstatus);
  yield takeEvery(actionTypes.USER_TO_DELETE, onDeleteUser);
}
