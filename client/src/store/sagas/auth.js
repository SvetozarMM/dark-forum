import { put, delay } from 'redux-saga/effects';
import * as actions from '../actions/index';
import axios from 'axios';
import jwtDecode from 'jwt-decode';
import * as constants from '../../constants/constants';
import * as storageService from '../../services/storage-service';
import * as notificator from '../../utils/notificator/notificator';

export function* logoutSaga(action) {
  yield storageService.remove('token');
  yield storageService.remove('expDate');
  yield put(actions.logoutSucceed());
}

export function* checkAuthTimeoutSaga(action) {
  const { payload: { expirationTime } } = action;
  yield delay(expirationTime);
  yield put(actions.logout());
}

export function* registerUserSaga(action) {
  const { payload } = action;
  const { username, email, password, url } = payload;

  try {
    yield axios.post(url, {
      username: username,
      email: email,
      password: password,
    });

    const loginUrl = `${constants.API_DOMAIN_NAME}/auth`;
    yield put(actions.auth(username, password, loginUrl, true));
  } catch (error) {
    yield notificator.error(`${error.response.data.error}`);
  }
}

export function* authUserSaga(action) {
  const { payload } = action;
  const { username, password, url, isAfterRegister } = payload;

  yield put(actions.authStart());

  try {
    const res = yield axios.post(url, {
      username: username,
      password: password,
    });

    if (isAfterRegister) {
      yield notificator.success(`Congratulations! You are member now.`);
    }

    const token = res.data.token;
    const decodedToken = yield jwtDecode(token);

    const userId = yield decodedToken.id;
    const userName = yield decodedToken.username;
    const userEmail = yield decodedToken.email;
    const isAdmin = yield decodedToken.roles.includes(constants.ADMIN_ROLE);
    const userBanstatus = yield decodedToken.banstatus;

    const expDate = yield new Date(
      new Date().getTime() + res.data.expiresIn * 1000
    );

    yield storageService.save('token', token);
    yield storageService.save('expDate', expDate);

    yield put(
      actions.authSuccess(
        token,
        userId,
        userName,
        userEmail,
        isAdmin,
        userBanstatus
      )
    );
    yield put(actions.checkAuthTimeout(res.data.expiresIn));
  } catch (error) {
    yield notificator.error(`${error.response.data.error}`);
  }
}

export function* authCheckStateSaga(action) {
  const token = yield storageService.read('token');

  if (!token) {
    yield put(actions.logout());
  } else {
    const decodedToken = yield jwtDecode(token);

    const userId = yield decodedToken.id;
    const userName = yield decodedToken.username;
    const userEmail = yield decodedToken.email;
    const isAdmin = yield decodedToken.roles.includes(constants.ADMIN_ROLE);
    const userBanstatus = yield decodedToken.banstatus;

    const expirationDate = yield new Date(storageService.read('expDate'));
    if (expirationDate <= new Date()) {
      yield put(actions.logout());
    } else {
      yield put(
        actions.authSuccess(
          token,
          userId,
          userName,
          userEmail,
          isAdmin,
          userBanstatus
        )
      );
      yield put(
        actions.checkAuthTimeout(
          (expirationDate.getTime() - new Date().getTime()) / 1000
        )
      );
    }
  }
}
