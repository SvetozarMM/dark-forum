import { put } from 'redux-saga/effects';
import * as postsService from '../../services/posts-service';
import * as storageService from '../../services/storage-service';
import * as actions from '../actions/index';
import * as notificator from '../../utils/notificator/notificator';

export function* onFetchAllPosts(action) {
  try {
    const token = storageService.read('token');
    const res = yield postsService.getAllPosts(token);
    if (res.data) {
      yield put(actions.saveAllPosts(res.data));
    }
  } catch (error) {
    yield notificator.error(`${error.response.data.error}`);
  }
}

export function* onFetchMyPosts(action) {
  try {
    const token = storageService.read('token');
    const res = yield postsService.getAllOwnPosts(token);
    if (res.data) {
      yield put(actions.saveMyPosts(res.data));
    }
  } catch (error) {
    yield notificator.error(`${error.response.data.error}`);
  }
}

export function* onUpdatePost(action) {
  const {
    payload: { post },
  } = action;

  try {
    const token = storageService.read('token');
    const res = yield postsService.updatePost(post, token);
    if (res.data) {
      yield put(actions.updatePost(res.data));
      yield notificator.success(`Your post has been successfully updated.`);
    }
  } catch (error) {
    yield notificator.error(`${error.response.data.error}`);
  }
}

export function* onLikePost(action) {
  const {
    payload: { post },
  } = action;

  try {
    const token = storageService.read('token');
    const res = yield postsService.likePost(post, token);
    if (res.data) {
      yield put(actions.likePost(res.data));
    }
  } catch (error) {
    yield notificator.error(`${error.response.data.error}`);
  }
}

export function* onToggleLockPost(action) {
  const {
    payload: { post },
  } = action;

  try {
    const token = storageService.read('token');
    let res;

    if (post.isLocked) {
      res = yield postsService.unlockPost(post, token);
    } else {
      res = yield postsService.lockPost(post, token);
    }

    if (res.data) {
      yield put(actions.lockUnlockPost(res.data));
      const msg = res.data.isLocked
        ? `Post has been locked.`
        : `Post has been unlocked.`;
      yield notificator.warning(msg);
    }
  } catch (error) {
    yield notificator.error(`${error.response.data.error}`);
  }
}

export function* onDeletePost(action) {
  const {
    payload: { postId },
  } = action;

  try {
    const token = storageService.read('token');
    const res = yield postsService.deletePost(postId, token);
    if (res.data) {
      yield put(actions.deletePost(postId));
      yield notificator.success(res.data.message);
    }
  } catch (error) {
    yield notificator.error(`${error.response.data.error}`);
  }
}

export function* onAddPost(action) {
  const {
    payload: { post },
  } = action;

  try {
    const token = storageService.read('token');
    const res = yield postsService.createPost(post, token);
    if (res.data) {
      yield put(actions.savePost(res.data));
      yield notificator.success(`Your post has been successfully submitted.`);
    }
  } catch (error) {
    yield notificator.error(`${error.response.data.error}`);
  }
}
