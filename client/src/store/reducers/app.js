import * as actionTypes from '../actions/actionTypes';
import { produce } from 'immer';

const initialState = {
  isCreateBoxOpen: false,
  isInSearchPostsMode: false,
};

export default produce((draftState, { type, payload } = {}) => {
  switch (type) {
    case actionTypes.TOGGLE_CREATE_POST: {
      draftState.isCreateBoxOpen = payload.isOpen;

      return;
    }
    case actionTypes.TOGGLE_SEARCH_MODE: {
      draftState.isInSearchPostsMode = payload.isInSearchMode;

      return;
    }

    default:
      return draftState;
  }
}, initialState);
