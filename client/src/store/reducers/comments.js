import * as actionTypes from '../actions/actionTypes';
import { produce } from 'immer';
import { filter, map } from 'ramda';

const initialState = {
  postComments: {},
  myComments: [],
};

const sortCommentsChronologicallyLatestLast = (comments) => {
  return comments.sort((a, b) =>
    new Date(a.createdOn) > new Date(b.createdOn) ? 1 : -1
  );
};

const sortCommentsChronologicallyLatestFirst = (comments) => {
  return comments.sort((a, b) =>
    new Date(a.createdOn) > new Date(b.createdOn) ? -1 : 1
  );
};

export default produce((draftState, { type, payload } = {}) => {
  switch (type) {
    case actionTypes.POST_COMMENTS: {
      const { postId, postComments } = payload;

      draftState.postComments[postId] = sortCommentsChronologicallyLatestLast(
        postComments
      );

      return;
    }

    case actionTypes.MY_COMMENTS: {
      const { myComments } = payload;

      draftState.myComments = sortCommentsChronologicallyLatestFirst(
        myComments
      );

      return;
    }

    case actionTypes.UPDATE_COMMENT: {
      const { comment, postId } = payload;

      draftState.postComments[postId] = map(
        (item) => (item.id === comment.id ? (item = { ...comment }) : item),
        draftState.postComments[postId]
      );

      return;
    }

    case actionTypes.LIKE_COMMENT: {
      const { comment, postId } = payload;

      draftState.postComments[postId] = map(
        (item) => (item.id === comment.id ? (item = { ...comment }) : item),
        draftState.postComments[postId]
      );

      return;
    }

    case actionTypes.DELETE_COMMENT: {
      const { commentId, postId } = payload;

      draftState.postComments[postId] = filter(
        (item) => item.id !== commentId,
        draftState.postComments[postId]
      );

      return;
    }

    case actionTypes.SAVE_COMMENT: {
      const { comment, postId } = payload;

      if (draftState.postComments[postId]) {
        draftState.postComments[postId].push(comment);
      } else {
        draftState.postComments[postId] = [comment];
      }

      return;
    }

    default:
      return draftState;
  }
}, initialState);
