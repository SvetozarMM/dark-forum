import * as actionTypes from '../actions/actionTypes';
import { produce } from 'immer';

const initialState = {
  token: null,
  isLogged: false,
  userId: null,
  userName: null,
  userEmail: null,
  isAdmin: false,
  userBanstatus: null,
};

export default produce((draftState, { type, payload } = {}) => {
  switch (type) {
    case actionTypes.AUTH_SUCCESS: {
      const {
        token,
        userId,
        userName,
        userEmail,
        isAdmin,
        userBanstatus,
      } = payload;

      draftState.token = token;
      draftState.isLogged = true;
      draftState.userId = userId;
      draftState.userName = userName;
      draftState.userEmail = userEmail;
      draftState.isAdmin = isAdmin;
      draftState.userBanstatus = userBanstatus;

      return;
    }

    case actionTypes.AUTH_LOGOUT: {
      draftState.token = null;
      draftState.isLogged = false;
      draftState.userId = null;
      draftState.userName = null;
      draftState.userEmail = null;
      draftState.isAdmin = false;
      draftState.userBanstatus = null;

      return;
    }

    default:
      return draftState;
  }
}, initialState);
