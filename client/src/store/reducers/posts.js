import * as actionTypes from '../actions/actionTypes';
import { produce } from 'immer';
import { filter, slice, map } from 'ramda';

const initialState = {
  allPosts: [],
  latestPosts: [],
  filteredPosts: [],
};

const sortPostChronologicallyLatestFirst = (posts) => {
  return posts.sort((a, b) =>
    new Date(a.createdOn) > new Date(b.createdOn) ? -1 : 1
  );
};

const getLatest = allPosts => slice(0, 6, allPosts);

export default produce((draftState, { type, payload } = {}) => {
  switch (type) {
    case actionTypes.ALL_POSTS: {
      const { allPosts } = payload;

      draftState.allPosts = sortPostChronologicallyLatestFirst(allPosts);
      draftState.latestPosts = getLatest(draftState.allPosts);

      return;
    }

    case actionTypes.ALL_FILTERED_POSTS: {
      const { filteredPosts } = payload;

      draftState.filteredPosts = sortPostChronologicallyLatestFirst(
        filteredPosts
      );

      return;
    }

    case actionTypes.MY_POSTS: {
      const { myPosts } = payload;

      draftState.allPosts = sortPostChronologicallyLatestFirst(myPosts);

      return;
    }

    case actionTypes.UPDATE_POST: {
      const { post } = payload;

      draftState.allPosts = map(
        (item) => (item.id === post.id ? (item = { ...post }) : item),
        draftState.allPosts
      );
      draftState.latestPosts = getLatest(draftState.allPosts);

      return;
    }

    case actionTypes.LIKE_POST: {
      const { post } = payload;

      draftState.allPosts = map(
        (item) => (item.id === post.id ? (item = { ...post }) : item),
        draftState.allPosts
      );

      return;
    }

    case actionTypes.LOCK_UNLOCK_POST: {
      const { post } = payload;

      draftState.allPosts = map(
        (item) => (item.id === post.id ? (item = { ...post }) : item),
        draftState.allPosts
      );

      return;
    }

    case actionTypes.SAVE_POST: {
      const { post } = payload;

      draftState.allPosts.unshift(post);
      draftState.latestPosts = getLatest(draftState.allPosts);

      return;
    }

    case actionTypes.DELETE_POST: {
      const { postId } = payload;

      draftState.allPosts = filter(
        (item) => item.id !== postId,
        draftState.allPosts
      );
      draftState.latestPosts = getLatest(draftState.allPosts);
      return;
    }

    default:
      return draftState;
  }
}, initialState);
