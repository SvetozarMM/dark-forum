import * as actionTypes from '../actions/actionTypes';
import { produce } from 'immer';
import { filter, map, toLower } from 'ramda';

const initialState = {
  userActivity: {},
  allUsers: [],
};

const sortUsersAlphabeticallyByName = (users) => {
  return users.sort((a, b) =>
  toLower(a.username) > toLower(b.username) ? 1 : -1
  );
};

export default produce((draftState, { type, payload } = {}) => {
  switch (type) {
    case actionTypes.USER_ACTIVITY: {
      const { userId, activity } = payload;
      draftState.userActivity[userId] = activity;

      return;
    }

    case actionTypes.ALL_USERS: {
      const { allUsers } = payload;
      draftState.allUsers = sortUsersAlphabeticallyByName(allUsers);

      return;
    }

    case actionTypes.USER_ROLE: {
      const { user } = payload;

      draftState.allUsers = map(
        (item) => (item.id === user.id ? (item = { ...user }) : item),
        draftState.allUsers
      );

      return;
    }

    case actionTypes.USER_BANSTATUS: {
      const { user } = payload;

      draftState.allUsers = map(
        (item) => (item.id === user.id ? (item = { ...user }) : item),
        draftState.allUsers
      );

      return;
    }

    case actionTypes.DELETE_USER: {
      const { user } = payload;

      draftState.allUsers = filter(
        (item) => item.id !== user.id,
        draftState.allUsers
      );

      return;
    }

    default:
      return draftState;
  }
}, initialState);
