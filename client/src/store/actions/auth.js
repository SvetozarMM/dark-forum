import * as actionTypes from './actionTypes';

export const authStart = () => {
  return {
    type: actionTypes.AUTH_START,
  };
};

export const authSuccess = (
  token,
  userId,
  userName,
  userEmail,
  isAdmin,
  userBanstatus
) => {
  return {
    type: actionTypes.AUTH_SUCCESS,
    payload: {
      token,
      userId,
      userName,
      userEmail,
      isAdmin,
      userBanstatus,
    },
  };
};

export const logout = () => {
  return {
    type: actionTypes.AUTH_INITIATE_LOGOUT,
  };
};

export const logoutSucceed = () => {
  return {
    type: actionTypes.AUTH_LOGOUT,
  };
};

export const checkAuthTimeout = (expirationTime) => {
  return {
    type: actionTypes.AUTH_CHECK_TIMEOUT,
    payload: { expirationTime },
  };
};

export const auth = (username, password, url, isAfterRegister) => {
  return {
    type: actionTypes.AUTH_USER,
    payload: {
      username,
      password,
      url,
      isAfterRegister,
    },
  };
};

export const register = (username, email, password, url) => {
  return {
    type: actionTypes.AUTH_REGISTER,
    payload: {
      username,
      email,
      password,
      url,
    }
  };
};

export const authCheckState = () => {
  return {
    type: actionTypes.AUTH_CHECK_STATE,
  };
};
