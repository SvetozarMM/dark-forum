import * as actionTypes from './actionTypes';

export const fetchPostComments = (postId) => {
  return {
    type: actionTypes.FETCH_POST_COMMENTS,
    payload: { postId },
  };
};

export const savePostComments = (postId, postComments) => {
  return {
    type: actionTypes.POST_COMMENTS,
    payload: { postId, postComments },
  };
};

export const fetchMyComments = () => {
  return {
    type: actionTypes.FETCH_MY_COMMENTS,
  };
};

export const saveMyComments = (myComments) => {
  return {
    type: actionTypes.MY_COMMENTS,
    payload: { myComments },
  };
};

export const updateSelectedComment = (comment, postId) => {
  return {
    type: actionTypes.UPDATE_COMMENT_SAGA,
    payload: { comment, postId },
  };
};

export const updateComment = (comment, postId) => {
  return {
    type: actionTypes.UPDATE_COMMENT,
    payload: { comment, postId },
  };
};

export const likeCommentUpdate = (comment, postId) => {
  return {
    type: actionTypes.LIKE_COMMENT_SAGA,
    payload: { comment, postId },
  };
};

export const likeComment = (comment, postId) => {
  return {
    type: actionTypes.LIKE_COMMENT,
    payload: { comment, postId },
  };
};

export const deleteCommentUpdate = (commentId, postId) => {
  return {
    type: actionTypes.DELETE_COMMENT_SAGA,
    payload: { commentId, postId },
  };
};

export const deleteComment = (commentId, postId) => {
  return {
    type: actionTypes.DELETE_COMMENT,
    payload: { commentId, postId },
  };
};

export const addComment = (comment, postId) => {
  return {
    type: actionTypes.ADD_COMMENT,
    payload: { comment, postId },
  };
};

export const saveComment = (comment, postId) => {
  return {
    type: actionTypes.SAVE_COMMENT,
    payload: { comment, postId },
  };
};
