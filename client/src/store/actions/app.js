import * as actionTypes from './actionTypes';

export const toggleCreatePost = (isOpen) => {
  return {
    type: actionTypes.TOGGLE_CREATE_POST,
    payload: { isOpen },
  };
};

export const toggleInSearchMode = (isInSearchMode) => {
  return {
    type: actionTypes.TOGGLE_SEARCH_MODE,
    payload: { isInSearchMode },
  };
};
