import * as actionTypes from './actionTypes';

export const fetchPosts = () => {
  return {
    type: actionTypes.FETCH_ALL_POSTS,
  };
};

export const saveAllPosts = (allPosts) => {
  return {
    type: actionTypes.ALL_POSTS,
    payload: { allPosts },
  };
};

export const saveFilteredPosts = (filteredPosts) => {
  return {
    type: actionTypes.ALL_FILTERED_POSTS,
    payload: { filteredPosts },
  };
};

export const fetchMyPosts = () => {
  return {
    type: actionTypes.FETCH_MY_POSTS,
  };
};

export const saveMyPosts = (myPosts) => {
  return {
    type: actionTypes.MY_POSTS,
    payload: { myPosts },
  };
};

export const updateSelectedPost = (post) => {
  return {
    type: actionTypes.UPDATE_POST_SAGA,
    payload: { post },
  };
};

export const updatePost = (post) => {
  return {
    type: actionTypes.UPDATE_POST,
    payload: { post },
  };
};

export const likePostUpdate = (post) => {
  return {
    type: actionTypes.LIKE_POST_SAGA,
    payload: { post },
  };
};

export const likePost = (post) => {
  return {
    type: actionTypes.LIKE_POST,
    payload: { post },
  };
};

export const lockUnlockPost = (post) => {
  return {
    type: actionTypes.LOCK_UNLOCK_POST,
    payload: { post },
  };
};

export const toggleLockPost = (post) => {
  return {
    type: actionTypes.TOGGLE_LOCK_POST,
    payload: { post },
  };
};

export const deletePostUpdate = (postId) => {
  return {
    type: actionTypes.DELETE_POST_SAGA,
    payload: { postId },
  };
};

export const deletePost = (postId) => {
  return {
    type: actionTypes.DELETE_POST,
    payload: { postId },
  };
};

export const addPost = (post) => {
  return {
    type: actionTypes.ADD_POST,
    payload: { post },
  };
}

export const savePost = (post) => {
  return {
    type: actionTypes.SAVE_POST,
    payload: { post },
  };
}
