import * as actionTypes from './actionTypes';

export const fetchUserActivity = (userId) => {
  return {
    type: actionTypes.FETCH_USER_ACTIVITY,
    payload: { userId },
  };
};

export const saveUserActiviity = (userId, activity) => {
  return {
    type: actionTypes.USER_ACTIVITY,
    payload: { userId, activity },
  };
};

export const fetchUsers = () => {
  return {
    type: actionTypes.FETCH_ALL_USERS,
  };
};

export const saveAllUsers = (allUsers) => {
  return {
    type: actionTypes.ALL_USERS,
    payload: { allUsers },
  };
};

export const updateUserRole = (user) => {
  return {
    type: actionTypes.UPDATE_USER_ROLE,
    payload: { user },
  };
};

export const saveUserRole = (user) => {
  return {
    type: actionTypes.USER_ROLE,
    payload: { user },
  };
};

export const updateUserBanstatus = (user) => {
  return {
    type: actionTypes.UPDATE_USER_BANSTATUS,
    payload: { user },
  };
};

export const saveUserBanstatus = (user) => {
  return {
    type: actionTypes.USER_BANSTATUS,
    payload: { user },
  };
};

export const userToDelete = (user) => {
  return {
    type: actionTypes.USER_TO_DELETE,
    payload: { user },
  };
};

export const deleteUser = (user) => {
  return {
    type: actionTypes.DELETE_USER,
    payload: { user },
  };
};
