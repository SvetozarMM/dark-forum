import React from 'react';
import './terms.css';
import DummyTextAbout from '../../assets/dummy-data/dummy-text-about';

const Terms = () => {
  return (
    <div className='terms-section-container'>
      <DummyTextAbout />
    </div>
  );
};

export default Terms;