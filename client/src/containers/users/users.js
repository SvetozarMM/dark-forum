import React, { useState, useEffect } from 'react';
import './users.css';
import { connect } from 'react-redux';
import { fetchUsers } from '../../store/actions/index';
import User from '../../components/user/user';
import Spinner from '../../components/UI/spinner/spinner';
import { map } from 'ramda';

const Users = (props) => {
  const { users, fetchUsers, loggedUserId } = props;

  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    fetchUsers();
    setIsLoading(false);
  }, [fetchUsers]);

  const allUsers = map((user) => {
    if (loggedUserId !== user.id) {
      return <User key={user.id} user={user} />;
    }
  }, users);

  return (
    <div className='controls-container-body'>
      {isLoading ? <Spinner /> : allUsers}
    </div>
  );
};

Users.defaultProps = {
  users: [],
  fetchUsers: () => {},
  loggedUserId: '',
};

export default connect(
  (state) => {
    return {
      users: state.users.allUsers,
      loggedUserId: state.auth.userId,
    };
  },
  { fetchUsers }
)(Users);
