import React, { Fragment } from 'react';
import './preferences.css';
import { connect } from 'react-redux';

const Preferences = (props) => {
  const { userName, userEmail, isAdmin } = props;

  return (
    <Fragment>
      <div className='preferences-section-container'>
        <div className='preferences-info-box'>
          <h3 className='heading heading__three preferences-text'>Username:</h3>
          <h3 className='heading heading__three preferences-info-text'>
            {userName}
          </h3>
        </div>

        <div className='preferences-info-box'>
          <h3 className='heading heading__three preferences-text'>Email:</h3>

          <h3 className='heading heading__three preferences-info-text'>
            {userEmail}
          </h3>
        </div>

        <div className='preferences-info-box'>
          <h3 className='heading heading__three preferences-text'>
            Forum role:
          </h3>

          <h3 className='heading heading__three preferences-info-text'>
            {isAdmin ? 'Admin' : 'Basic'}
          </h3>
        </div>
      </div>
    </Fragment>
  );
};

Preferences.defaultProps = {
  userName: '',
  userEmail: '',
  isAdmin: false,
}

export default connect((state) => {
  return {
    userName: state.auth.userName,
    userEmail: state.auth.userEmail,
    isAdmin: state.auth.isAdmin,
  };
})(Preferences);
