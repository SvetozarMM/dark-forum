import React, { Fragment } from 'react';
import SectionContainer from '../../components/section-container/section-container';
import SectionHeading from '../../components/section-container/section-heading/section-heading';
import SectionBody from '../../components/section-container/section-body/section-body';
import ControlsOptions from '../../components/controls-options/controls-options';
import Users from '../users/users';

const Controls = () => {
  return (
    <Fragment>
      <SectionContainer>
        <SectionHeading>
          <ControlsOptions />
        </SectionHeading>
        <SectionBody>
          <Users />
        </SectionBody>
      </SectionContainer>
    </Fragment>
  );
};

export default Controls;
