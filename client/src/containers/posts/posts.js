import React, { useEffect, Fragment, useState } from 'react';
import { connect } from 'react-redux';
import {
  fetchPosts,
  fetchMyPosts,
  updateSelectedPost,
  likePostUpdate,
  toggleLockPost,
  deletePostUpdate,
  addPost,
} from '../../store/actions/index';
import Post from '../../components/post/post';
import AddPost from '../../components/UI/add-post/add-post';
import Spinner from '../../components/UI/spinner/spinner';
import './posts.css';
import { map, isEmpty } from 'ramda';

const Posts = (props) => {
  const {
    fetchPosts,
    fetchMyPosts,
    isUserLogged,
    posts,
    updateSelectedPost,
    likePostUpdate,
    toggleLockPost,
    deletePostUpdate,
    addPost,
    isCreateBoxOpen,
    isMyPosts,
    filteredPosts,
    isInSearchPostsMode,
  } = props;

  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    if (isUserLogged && !isMyPosts && !isInSearchPostsMode) {
      fetchPosts();
      setIsLoading(false);
    }
    if (isUserLogged && isMyPosts && !isInSearchPostsMode) {
      fetchMyPosts();
      setIsLoading(false);
    }
  }, [fetchPosts, fetchMyPosts, isMyPosts, isUserLogged, isInSearchPostsMode]);

  const toggleLockPostHandler = (post) => toggleLockPost(post);
  const updatePost = (post) => updateSelectedPost(post);
  const likePost = (post) => likePostUpdate(post);
  const deletePost = (postId) => deletePostUpdate(postId);
  const addPostHandler = (post) => addPost(post);

  const postToDisplay = !isInSearchPostsMode ? posts : filteredPosts;

  const allPosts = map((post) => {
    return (
      <Post
        key={post.id}
        post={post}
        update={updatePost}
        like={likePost}
        toggleLock={toggleLockPostHandler}
        deletePost={deletePost}
        create={addPostHandler}
      />
    );
  }, postToDisplay);

  return (
    <Fragment>
      <div>
        {isCreateBoxOpen ? <AddPost savePost={addPostHandler} /> : null}
      </div>
      <div className='posts-container'>
        {isLoading ? (
          <Spinner />
        ) : isInSearchPostsMode && isEmpty(allPosts) ? (
          <h1>Nothing found!</h1>
        ) : !isEmpty(allPosts) ? (
          allPosts
        ) : isMyPosts && isEmpty(allPosts) ? (
          <h3 className='heading heading__three heading-empty-container-msg'>
            No posts yet!
          </h3>
        ) : null}
      </div>
    </Fragment>
  );
};

Posts.defaultProps = {
  fetchPosts: () => {},
  fetchMyPosts: () => {},
  isUserLogged: false,
  posts: [],
  updateSelectedPost: () => {},
  likePostUpdate: () => {},
  toggleLockPost: () => {},
  deletePostUpdate: () => {},
  addPost: () => {},
  isCreateBoxOpen: false,
  isMyPosts: false,
  filteredPosts: [],
  isInSearchPostsMode: false,
};

export default connect(
  (state) => {
    return {
      posts: state.posts.allPosts,
      isUserLogged: state.auth.isLogged,
      isCreateBoxOpen: state.app.isCreateBoxOpen,
      filteredPosts: state.posts.filteredPosts,
      isInSearchPostsMode: state.app.isInSearchPostsMode,
    };
  },
  {
    fetchPosts,
    fetchMyPosts,
    updateSelectedPost,
    likePostUpdate,
    toggleLockPost,
    deletePostUpdate,
    addPost,
  }
)(Posts);
