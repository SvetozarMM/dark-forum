import React, { useEffect } from 'react';
import { logout } from '../../store/actions/index';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import * as notificator from '../../utils/notificator/notificator';

const Logout = (props) => {
  const { logout } = props;

  useEffect(() => {
    logout();
    notificator.success('Successful Logout!');
  });

  return <Redirect to='/' />;
};

Logout.defaultProps = {
  logout: () => {},
};

export default connect(null, { logout })(Logout);
