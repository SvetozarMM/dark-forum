import React from 'react';
import AuthForm from '../../components/auth-form/auth-form';
import { auth, register } from '../../store/actions/index';
import { connect } from 'react-redux';
import * as constants from '../../constants/constants';

const Auth = (props) => {
  const { auth, register } = props;

  const authHandlerRegister = (username, email, password) => {
    const url = `${constants.API_DOMAIN_NAME}/users`;
    register(username, email, password, url);
  };

  const authHandlerSignIn = (username, password) => {
    const url = `${constants.API_DOMAIN_NAME}/auth`;
    auth(username, password, url);
  };

  return (
    <AuthForm
      authHandlerSignIn={authHandlerSignIn}
      authHandlerRegister={authHandlerRegister}
    />
  );
};

Auth.defaultProps = {
  auth: () => {},
  register: () => {},
};

export default connect(null, { register, auth })(Auth);
