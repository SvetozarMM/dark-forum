import React, { Fragment } from 'react';
import Posts from '../../posts/posts';
import SectionContainer from '../../../components/section-container/section-container';
import SectionHeading from '../../../components/section-container/section-heading/section-heading';
import SectionBody from '../../../components/section-container/section-body/section-body';
import ActivityOptions from '../../../components/activity-options/activity-options';

const MyPosts = (props) => {
  const isMyPosts = true;

  return (
    <Fragment>
      <SectionContainer>
        <SectionHeading>
          <ActivityOptions />
        </SectionHeading>
        <SectionBody>
          <Posts isMyPosts={isMyPosts} />
        </SectionBody>
      </SectionContainer>
    </Fragment>
  );
};

export default MyPosts;
