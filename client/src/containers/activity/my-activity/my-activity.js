import React, { useEffect } from 'react';
import './my-activity.css';
import ActivityBox from '../../../components/UI/activity-box/activity-box';
import { connect } from 'react-redux';
import { fetchUserActivity } from '../../../store/actions/index';
import { isEmpty } from 'ramda';

const MyActivity = (props) => {
  const { userId, fetchUserActivity, userActivity } = props;

  useEffect(() => {
    if (userId) {
      fetchUserActivity(userId);
    }
  }, [fetchUserActivity, userId]);

  const activity = userActivity[userId]?.map((item) => (
    <ActivityBox key={item.id} activityItem={item} />
  ));

  return (
    <div className='my-activity-list-container'>
      {!isEmpty(activity) ? (
        activity
      ) : (
        <h3 className='heading heading__three heading-empty-container-msg'>
          No activity yet!
        </h3>
      )}
    </div>
  );
};

MyActivity.defaultProps = {
  userId: '',
  fetchUserActivity: () => {},
  userActivity: {},
};

export default connect(
  (state) => {
    return {
      userActivity: state.users.userActivity,
    };
  },
  { fetchUserActivity }
)(MyActivity);
