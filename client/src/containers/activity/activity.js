import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import MyActivity from './my-activity/my-activity';
import SectionContainer from '../../components/section-container/section-container';
import SectionHeading from '../../components/section-container/section-heading/section-heading';
import SectionBody from '../../components/section-container/section-body/section-body';
import ActivityOptions from '../../components/activity-options/activity-options';

const Activity = (props) => {
  const { loggedUserId } = props;

  return (
    <Fragment>
      <SectionContainer>
        <SectionHeading>
          <ActivityOptions />
        </SectionHeading>
        <SectionBody>
          <MyActivity userId={loggedUserId} />
        </SectionBody>
      </SectionContainer>
    </Fragment>
  );
};

Activity.defaultProps = {
  loggedUserId: '',
};

export default connect((state) => {
  return {
    loggedUserId: state.auth.userId,
  };
})(Activity);
