import React, { Fragment } from 'react';
import Comments from '../../comments/comments';
import SectionContainer from '../../../components/section-container/section-container';
import SectionHeading from '../../../components/section-container/section-heading/section-heading';
import SectionBody from '../../../components/section-container/section-body/section-body';
import ActivityOptions from '../../../components/activity-options/activity-options';

const MyComments = () => {
  const isMyComments = true;

  return (
    <Fragment>
      <SectionContainer>
        <SectionHeading>
          <ActivityOptions />
        </SectionHeading>
        <SectionBody>
          <Comments isMyComments={isMyComments} />
        </SectionBody>
      </SectionContainer>
    </Fragment>
  );
};

export default MyComments;
