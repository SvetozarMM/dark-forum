import React, { useEffect, useState } from 'react';
import './comments.css';
import { connect } from 'react-redux';
import {
  fetchMyComments,
  fetchPostComments,
  updateSelectedComment,
  likeCommentUpdate,
  deleteCommentUpdate,
} from '../../store/actions/index';
import Spinner from '../../components/UI/spinner/spinner';
import Comment from '../../components/comment/comment';
import { isEmpty } from 'ramda';

const Comments = (props) => {
  const {
    fetchMyComments,
    fetchPostComments,
    myComments,
    postComments,
    isUserLogged,
    isMyComments,
    postOwnerId,
    updateSelectedComment,
    likeCommentUpdate,
    deleteCommentUpdate,
  } = props;

  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    if (isUserLogged && isMyComments) {
      fetchMyComments();
      setIsLoading(false);
    }
    if (isUserLogged && !isMyComments) {
      fetchPostComments(postOwnerId);
      setIsLoading(false);
    }
  }, [
    fetchMyComments,
    isUserLogged,
    isMyComments,
    setIsLoading,
    fetchPostComments,
    postOwnerId,
  ]);

  const updateComment = (comment) =>
    updateSelectedComment(comment, postOwnerId);
  const likeComment = (comment) => likeCommentUpdate(comment, postOwnerId);
  const deleteComment = (commentId) =>
    deleteCommentUpdate(commentId, postOwnerId);

  const allOwnComments = myComments?.map((comment) => (
    <Comment
      key={comment.id}
      comment={comment}
      isMyCommentsMode={isMyComments}
    />
  ));

  const allPostComments = postComments[postOwnerId]?.map((comment) => (
    <Comment
      key={comment.id}
      comment={comment}
      update={updateComment}
      like={likeComment}
      deleteComment={deleteComment}
      isMyCommentsMode={isMyComments}
    />
  ));

  return (
    <div className='comments-list'>
      {isLoading ? (
        <Spinner />
      ) : isMyComments && !isEmpty(allOwnComments) ? (
        allOwnComments
      ) : !isMyComments && !isEmpty(allPostComments) ? (
        allPostComments
      ) : (
        <h3 className='heading heading__three heading-empty-container-msg'>
          No comments yet!
        </h3>
      )}
    </div>
  );
};

Comments.defaultProps = {
  fetchMyComments: () => {},
  fetchPostComments: () => {},
  myComments: [],
  postComments: {},
  isUserLogged: false,
  isMyComments: false,
  postOwnerId: '',
  updateSelectedComment: () => {},
  likeCommentUpdate: () => {},
  deleteCommentUpdate: () => {},
};

export default connect(
  (state) => {
    return {
      isUserLogged: state.auth.isLogged,
      myComments: state.comments.myComments,
      postComments: state.comments.postComments,
    };
  },
  {
    fetchMyComments,
    fetchPostComments,
    updateSelectedComment,
    likeCommentUpdate,
    deleteCommentUpdate,
  }
)(Comments);
