import React from 'react';
import './about.css';
import DummyTextAbout from '../../assets/dummy-data/dummy-text-about';

const About = () => {
  return (
    <div className='about-section-container'>
      <DummyTextAbout />
    </div>
  );
};

export default About;
