import React, { useEffect } from 'react';
import './App.css';
import { connect } from 'react-redux';
import { authCheckState } from './store/actions/index';
import AuthPage from './hoc/auth-page/auth-page';
import MainPage from './hoc/main-page/main-page';
import MainContainer from './components/main-container/main-container';
import ReactNotification from 'react-notifications-component';
import 'react-notifications-component/dist/theme.css';

const App = (props) => {
  const { isUserLogged, authCheckState } = props;

  useEffect(() => {
    authCheckState();
  }, [authCheckState]);

  return (
    <div>
      <MainContainer>
        <ReactNotification />
        {isUserLogged ? <MainPage /> : <AuthPage />}
      </MainContainer>
    </div>
  );
};

export default connect(
  (state) => {
    return {
      isUserLogged: state.auth.isLogged,
    };
  },
  { authCheckState }
)(App);
