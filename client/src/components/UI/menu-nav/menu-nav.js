import React from 'react'
import './menu-nav.css';

const MenuNav = (props) => {
  return (
    <div className='menu-nav-container'>
      {props.children}
    </div>
  )
}

export default MenuNav;
