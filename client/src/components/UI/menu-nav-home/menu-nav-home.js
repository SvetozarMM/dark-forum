import React, { Fragment } from 'react'
import './menu-nav-home.css';

const MenuNavHome = (props) => {
  return (
    <Fragment>
      <div className='menu-nav-home-container'>{props.children}</div>
    </Fragment>
  );
}

export default MenuNavHome;
