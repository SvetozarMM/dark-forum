import React, { Fragment } from 'react';
import './user-info-box.css';
import { connect } from 'react-redux';
import { toUpper } from 'ramda';

const UserInfoBox = (props) => {
  const { userName } = props;

  return (
    <Fragment>
      <div className='user-info-box'>
        <div className='user-avatar'>
          <h1 className='heading-avatar-text'>
            {toUpper(userName?.charAt(0))}
          </h1>
        </div>

        <h2 className='user-name-text'>{userName}</h2>
      </div>
    </Fragment>
  );
};

UserInfoBox.defaultProps = {
  userName: '',
};

export default connect((state) => {
  return {
    userName: state.auth.userName,
  };
})(UserInfoBox);
