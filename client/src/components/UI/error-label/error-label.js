import React from 'react'
import classes from './error-label.module.css';

const ErrorLabel = (props) => {
  return (
    <label
      className={classes.label}
      style={{
        opacity: props.show ? 0 : 1,
      }}
    >
      {props.msg}
    </label>
  );
}

export default ErrorLabel;
