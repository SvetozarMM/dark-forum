import React from 'react';
import './activity-box.css';
import moment from 'moment';

const ActivityBox = (props) => {

  const { activityItem } = props;

  return (
    <div className='single-activity-item-box'>
      <h4 className='heading heading__four'>
        {activityItem.userActivity}&nbsp;on:&nbsp;
      </h4>
      <h4 className='heading heading__four heading__four--date'>
        {moment(activityItem.createdOn).format('DD/MM/YYYY - hh:mm:ss')}
      </h4>
    </div>
  );
};

ActivityBox.defaultProps = {
  activityItem: {},
}

export default ActivityBox;
