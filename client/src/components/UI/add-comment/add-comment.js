import React, { Fragment, useState, useCallback } from 'react';
import { connect } from 'react-redux';
import './add-comment.css';
import AuthorBox from '../author-box/author-box';

const AddComment = (props) => {
  const { userName, closeCreateBox, saveComment } = props;

  const [commentContent, setCommentContent] = useState('');

  const commentContentHandler = useCallback(
    (event) => setCommentContent(event.target.value),
    []
  );

  const saveCommentHandler = useCallback(() => {
    saveComment({
      content: commentContent,
    });
    closeCreateBox();
  }, [saveComment, commentContent, closeCreateBox]);

  return (
    <Fragment>
      <div className='add-comment-box'>
        <div className='add-comment-box__header'>
          <AuthorBox authorName={userName} size='small' />
        </div>

        <div className='add-comment-box__body'>
          <textarea
            className='add-comment-content-input'
            type='text'
            value={commentContent}
            placeholder='Your comment'
            onChange={commentContentHandler}
          />
        </div>

        <div className='add-comment-edit-mode-options'>
          <h3
            className='heading heading__three add-comment-save'
            onClick={saveCommentHandler}
          >
            Save
          </h3>
          <span className='add-comment-slash'>|</span>
          <h3
            className='heading heading__three add-comment-cancel'
            onClick={closeCreateBox}
          >
            Cancel
          </h3>
        </div>
      </div>
    </Fragment>
  );
};

AddComment.defaultProps = {
  userName: '',
  closeCreateBox: () => {},
  saveComment: () => {},
};

export default connect((state) => {
  return {
    userName: state.auth.userName,
  };
})(AddComment);
