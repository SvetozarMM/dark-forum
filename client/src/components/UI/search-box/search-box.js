import React, { useState, useEffect, useCallback } from 'react';
import './search-box.css';
import { Icon } from '@material-ui/core';
import { Search } from '@material-ui/icons';
import { connect } from 'react-redux';
import { toggleInSearchMode, saveFilteredPosts } from '../../../store/actions/index';
import { filter, toLower, includes } from 'ramda';

const SearchBox = (props) => {
  const {
    allPosts,
    saveFilteredPosts,
    toggleInSearchMode,
  } = props;

  const [postTitleToSearch, setPostTitleToSearch] = useState('');

  const searchPostByTitle = useCallback(() => {
    const filteredPosts = filter(
      (post) => includes(toLower(postTitleToSearch), toLower(post.title)),
      allPosts
    );

    saveFilteredPosts(filteredPosts);
  }, [allPosts, postTitleToSearch, saveFilteredPosts]);

  const checkSearchMode = useCallback(() => {
    if (postTitleToSearch.length >= 1) {
      toggleInSearchMode(true);
      searchPostByTitle();
    } else {
      toggleInSearchMode(false);
      saveFilteredPosts([]);
    }
  }, [
    postTitleToSearch,
    toggleInSearchMode,
    searchPostByTitle,
    saveFilteredPosts,
  ]);

  useEffect(() => {
    checkSearchMode();
  }, [checkSearchMode]);

  const postTitleHandler = (event) => {
    setPostTitleToSearch(event.target.value);
  };

  return (
    <div className='search-bar'>
      <input
        type='text'
        className='search-bar-input'
        placeholder='Search Post'
        value={postTitleToSearch}
        onChange={postTitleHandler}
      />
      <button
        type='submit'
        className='btns btns-search'
        onClick={searchPostByTitle}
      >
        <Icon className='btns-search-icon' component={Search} />
      </button>
    </div>
  );
};

SearchBox.defaultProps = {
  allPosts: [],
  saveFilteredPosts: () => {},
  toggleInSearchMode: () => {},
};

export default connect(
  (state) => {
    return {
      allPosts: state.posts.allPosts,
    };
  },
  {
    toggleInSearchMode,
    saveFilteredPosts,
  }
)(SearchBox);
