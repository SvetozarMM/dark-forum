import React from 'react';
import './btn-add-post.css';
import '../../../assets/css/btns-style/btns-style.css';
import { connect } from 'react-redux';
import { toggleCreatePost } from '../../../store/actions/index';
import { withRouter } from 'react-router-dom';
import * as notificator from '../../../utils/notificator/notificator';

const BtnAddPost = (props) => {
  const { toggleCreatePost, history, isUserBanned } = props;

  const openCreatePostHandler = () => {
    if (!isUserBanned) {
      toggleCreatePost(true);
      history.push('/posts');
    } else {
      notificator.warning(`You are temporary banned!`);
    }
  };

  return (
    <div>
      <button
        className='btns btn-add-post'
        onClick={openCreatePostHandler}
        disabled={isUserBanned}
        title={isUserBanned ? 'You are temporary banned!' : 'Add Post'}
      >
        Add Post
      </button>
    </div>
  );
};

BtnAddPost.defaultProps = {
  onOpenCreatePost: () => {},
  history: [],
  isUserBanned: false,
};

export default connect(
  (state) => {
    return {
      isUserBanned: state.auth.userBanstatus?.isBanned,
    };
  },
  {
    toggleCreatePost,
  }
)(withRouter(BtnAddPost));
