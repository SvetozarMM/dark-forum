import React, { Fragment } from 'react';
import './author-box.css';
import { toUpper } from 'ramda';

const AuthorBox = (props) => {
  const { authorName, size } = props;

  return (
    <Fragment>
      <div className={`author-box ${size}`}>
        <div className={`author-avatar ${size}`}>
          <h1 className={`author-text ${size}`}>
            {toUpper(authorName?.charAt(0))}
          </h1>
        </div>

        <h2 className={`author-name-text ${size}`}>{authorName}</h2>
      </div>
    </Fragment>
  );
};

AuthorBox.defaultProps = {
  authorName: '',
  size: '',
}

export default AuthorBox;
