import React, { Fragment, useState, useCallback } from 'react';
import './add-post.css';
import AuthorBox from '../author-box/author-box';
import { connect } from 'react-redux';
import { toggleCreatePost } from '../../../store/actions/index';

const AddPost = (props) => {
  const { userName, savePost, toggleCreatePost } = props;

  const [postTitle, setPostTitle] = useState('');
  const [postContent, setPostContent] = useState('');

  const postTitleHandler = useCallback(
    (event) => setPostTitle(event.target.value),
    []
  );
  const postContentHandler = useCallback(
    (event) => setPostContent(event.target.value),
    []
  );
  const closeCreatePostHandler = useCallback(() => toggleCreatePost(false), [
    toggleCreatePost,
  ]);

  const savePostHandler = useCallback(() => {
    savePost({
      title: postTitle,
      content: postContent,
    });
    toggleCreatePost(false);
  }, [savePost, postTitle, postContent, toggleCreatePost]);

  return (
    <Fragment>
      <div className='add-post-box '>
        <div className='add-post-box__header'>
          <AuthorBox authorName={userName} size='big' />
          <textarea
            className='add-title-input'
            placeholder='Post title'
            value={postTitle}
            onChange={postTitleHandler}
          ></textarea>
        </div>

        <div className='add-post-box__body'>
          <textarea
            className='add-content-input'
            placeholder='Your post'
            value={postContent}
            onChange={postContentHandler}
          ></textarea>
        </div>

        <div className='add-mode-options'>
          <h3
            className='heading heading__three add-save-post'
            onClick={savePostHandler}
          >
            Save
          </h3>
          <span className='add-slash'>|</span>
          <h3
            className='heading heading__three add-cancel-post'
            onClick={closeCreatePostHandler}
          >
            Cancel
          </h3>
        </div>
      </div>
    </Fragment>
  );
};

AddPost.defaultProps = {
  userName: '',
  savePost: () => {},
  onCloseCreatePost: () => {},
};

export default connect(
  (state) => {
    return {
      userName: state.auth.userName,
    };
  },
  {
    toggleCreatePost,
  }
)(AddPost);
