import React from 'react';
import './nav-btn.css';
import { NavLink } from 'react-router-dom';

const NavBtn = (props) => {
  const { text, link, active } = props;

  return (
    <div className='navigation-item'>
      <NavLink
        className={active ? 'active' : 'navigation-item-link'}
        to={link}
        activeClassName='active'
      >
        <h4>{text}</h4>
      </NavLink>
    </div>
  );
};

export default NavBtn;
