import React from 'react'
import './logo.css';
import LogoImg from '../../../assets/imgs/chat.png';

const Logo = () => {
  return (
    <div className='logo-box'>
      <img
        className='logo-img'
        src={LogoImg}
        alt='logo'
      />
    </div>
  )
}

export default Logo;
