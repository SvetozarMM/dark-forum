import React from 'react';
import './section-action.css';
import { NavLink } from 'react-router-dom';

const SectionAction = (props) => {
  const { name, active, link } = props;

  return (
    <NavLink
      className={active ? 'section-item-active' : 'section-item-link '}
      to={link}
      activeClassName='section-item-active'
    >
      {name}
    </NavLink>
  );
};

export default SectionAction;
