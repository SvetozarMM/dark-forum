import React, { useState, useCallback } from 'react';
import './nav-menu.css';
import NavMenuActions from './nav-menu-actions/nav-menu-actions';
import { Icon } from '@material-ui/core';
import { Menu, Close } from '@material-ui/icons';

const NavMenu = () => {
  const [isMenuOpen, setIsMenuOpen] = useState(false);

  const menuOpenHandler = useCallback(() => setIsMenuOpen(!isMenuOpen), [isMenuOpen]);

  return (
    <div className='menu-container'>
      <input
        type='checkbox'
        className='menu__checkbox'
        id='menu'
        checked={isMenuOpen}
        readOnly
      />
      <label htmlFor='menu' className='menu__btn--open'>
        <Icon
          className='menu__icon'
          fontSize='large'
          component={Menu}
          onClick={menuOpenHandler}
        />
      </label>
      <label htmlFor='menu' className='menu__btn--close'>
        <Icon
          className='menu__icon'
          fontSize='large'
          component={Close}
          onClick={menuOpenHandler}
        />
      </label>

      <div className='menu__bgr'>&nbsp;</div>

      <nav className='menu__nav'>
        <NavMenuActions />
      </nav>

      {isMenuOpen ? (
        <div className='backdrop' onClick={menuOpenHandler}></div>
      ) : null}
    </div>
  );
};

export default NavMenu;
