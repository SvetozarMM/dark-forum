import React from 'react';
import './nav-menu-action.css';
import { NavLink } from 'react-router-dom';
import { Icon } from '@material-ui/core';

const NavMenuAction = (props) => {
  const { name, active, link, icon } = props;

  return (
    <li className='nav-actions-item'>
      <Icon className='nav-action-icon' component={icon} />
      <NavLink
        className={active ? 'active' : 'navigation-item-link'}
        to={link}
        activeClassName='active'
      >
        {name}
      </NavLink>
    </li>
  );
};

export default NavMenuAction;
