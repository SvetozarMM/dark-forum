import React from 'react';
import './nav-menu-actions.css';
import NavMenuAction from './nav-menu-action/nav-menu-action';
import {
  RemoveRedEye,
  LibraryBooks,
  BarChart,
  Info,
  Settings,
  ExitToApp,
} from '@material-ui/icons';
import { connect } from 'react-redux';

const NavMenuActions = (props) => {
  const { isAdmin } = props;

  return (
    <div>
      <ul className='nav-actions'>
        <NavMenuAction name='Posts' icon={LibraryBooks} link={'./posts'} />
        <NavMenuAction name='Activity' icon={BarChart} link={'./activity'} />
        <NavMenuAction name='About' icon={Info} link={'./about'} />
        <NavMenuAction
          name='Preferences'
          icon={Settings}
          link={'./preferences'}
        />
        {isAdmin ? (
          <NavMenuAction
            name='Controls'
            icon={RemoveRedEye}
            link={'./controls'}
          />
        ) : null}

        <NavMenuAction name='Logout' link={'./logout'} icon={ExitToApp} />
      </ul>
    </div>
  );
};

NavMenuActions.defaultProps = {
  isAdmin: false,
};

export default connect((state) => {
  return {
    isAdmin: state.auth.isAdmin,
  };
})(NavMenuActions);
