import React, { Fragment, useState } from 'react';
import './user.css';
import AuthorBox from '../UI/author-box/author-box';
import { Icon } from '@material-ui/core';
import {
  Info,
  LibraryBooks,
  SupervisedUserCircle,
  PersonAddDisabled,
  DeleteForever,
} from '@material-ui/icons';
import UserInfo from '../user-content/user-info/user-info';
import UserActivity from '../user-content/user-activity/user-activity';
import UserRole from '../user-content/user-role/user-role';
import UserBanstatus from '../user-content/user-banstatus/user-banstatus';
import UserDelete from '../user-content/user-delete/user-delete';

const User = (props) => {
  const { user } = props;

  const [sectionsVisibility, setSectionsVisibility] = useState({
    isInfo: false,
    isActivity: false,
    isRole: false,
    isBanstatus: false,
    isDelete: false,
  });

  const sectionVisibilityHandler = (sectionName) => {
    setSectionsVisibility({
      ...{
        isInfo: false,
        isActivity: false,
        isRole: false,
        isBanstatus: false,
        isDelete: false,
      },
      [sectionName]: !sectionsVisibility[sectionName],
    });
  };

  let optionComponent = sectionsVisibility.isInfo ? (
    <UserInfo user={user} />
  ) : sectionsVisibility.isActivity ? (
    <UserActivity currentUserId={user.id} />
  ) : sectionsVisibility.isRole ? (
    <UserRole user={user} closeBox={() => sectionVisibilityHandler('isRole')} />
  ) : sectionsVisibility.isBanstatus ? (
    <UserBanstatus user={user} />
  ) : sectionsVisibility.isDelete ? (
    <UserDelete user={user} closeBox={() => sectionVisibilityHandler('isDelete')} />
  ) : null;

  return (
    <Fragment>
      <div className='users-container'>
        <div className='user-container__heading'>
          <AuthorBox authorName={user.username} size='small' />

          <div className='users-container__options'>
            <Icon
              className='users-container__options-icon'
              fontSize='large'
              titleAccess='User Info'
              component={Info}
              onClick={() => sectionVisibilityHandler('isInfo')}
            />
            <Icon
              className='users-container__options-icon'
              fontSize='large'
              titleAccess='User Activity'
              component={LibraryBooks}
              onClick={() => sectionVisibilityHandler('isActivity')}
            />
            <Icon
              className='users-container__options-icon'
              titleAccess='User role'
              fontSize='large'
              component={SupervisedUserCircle}
              onClick={() => sectionVisibilityHandler('isRole')}
            />
            <Icon
              className='users-container__options-icon'
              titleAccess='User Banstatus'
              fontSize='large'
              component={PersonAddDisabled}
              onClick={() => sectionVisibilityHandler('isBanstatus')}
            />
            <Icon
              className='users-container__options-icon users-container__options-icon--remove'
              titleAccess='Delete User'
              fontSize='large'
              component={DeleteForever}
              onClick={() => sectionVisibilityHandler('isDelete')}
            />
          </div>
        </div>

        <div className='user-options-hero-container'>{optionComponent}</div>
      </div>
    </Fragment>
  );
};

User.defaultProps = {
  user: {},
}

export default User;
