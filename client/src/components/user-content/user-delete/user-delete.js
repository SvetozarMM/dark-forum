import React, { Fragment } from 'react';
import './user-delete.css';
import { connect } from 'react-redux';
import { userToDelete } from '../../../store/actions/index';

const UserDelete = (props) => {
  const { user, userToDelete, closeBox } = props;

  const deleteUserHandler = () => userToDelete(user);
  const closeDeleteBoxHandler = () => closeBox();

  return (
    <Fragment>
      <div className='user-delete-content-box'>
        <h4 className='heading heading__three'>
          Delete{' '}
          <span className='user-delete-content-name'>{user.username}</span>{' '}
          permanently?
        </h4>
        <div className='user-delete-content-options'>
          <h3
            className='heading heading__three user-delete-content-btns'
            onClick={deleteUserHandler}
          >
            YES
          </h3>
          <span className='user-delete-content-slash'>|</span>
          <h3
            className='heading heading__three user-delete-content-btns'
            onClick={closeDeleteBoxHandler}
          >
            CANCEL
          </h3>
        </div>
      </div>
    </Fragment>
  );
};

UserDelete.defaultProps = {
  user: {},
  userToDelete: () => {},
  closeBox: () => {},
}

export default connect(null, { userToDelete })(UserDelete);
