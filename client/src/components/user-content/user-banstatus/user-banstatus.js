import React, { Fragment, useState } from 'react';
import './user-banstatus.css';
import { connect } from 'react-redux';
import { updateUserBanstatus } from '../../../store/actions/index';

const UserBanstatus = (props) => {
  const { user, updateUserBanstatus } = props;

  const [statusDescription, setStatusDescription] = useState(
    user?.banstatus.description
  );

  const statusDescriptionHandler = (event) => {
    setStatusDescription(event.target.value);
  };

  const banUserHandler = () => {
    updateUserBanstatus({
      ...user,
      banstatus: {
        isBanned: true,
        description: statusDescription,
      },
    });
  };

  const permitUserHandler = () => {
    updateUserBanstatus({
      ...user,
      banstatus: {
        isBanned: false,
        description: '',
      },
    });
    setStatusDescription('');
  };

  return (
    <Fragment>
      <div className='user-banstatus-content-box'>
        <div className='user-banstatus-content-info-box'>
          <h4 className='heading heading__four'>Banstatus:</h4>
          <h4 className='heading heading__four user-banstatus-content-text'>
            {user?.banstatus.isBanned ? 'User is Banned' : 'OK'}
          </h4>
        </div>
        <div className='user-banstatus-content-info-box'>
          <h4 className='heading heading__four'>Description:</h4>
          <textarea
            className='user-banstatus-content-input'
            value={statusDescription}
            onChange={statusDescriptionHandler}
          ></textarea>
        </div>

        <div className='user-banstatus-content-options'>
          <h3
            className='heading heading__three user-banstatus-content-btns'
            onClick={banUserHandler}
          >
            Ban
          </h3>
          <span className='user-banstatus-content-slash'>|</span>
          <h3
            className='heading heading__three user-banstatus-content-btns'
            onClick={permitUserHandler}
          >
            Permit
          </h3>
        </div>
      </div>
    </Fragment>
  );
};

UserBanstatus.defaultProps = {
  user: {},
  updateUserBanstatus: () => {},
};

export default connect(null, { updateUserBanstatus })(UserBanstatus);
