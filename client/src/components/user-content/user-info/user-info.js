import React, {Fragment} from 'react'
import './user-info.css';

const UserInfo = (props) => {

  const {user} = props;

  return (
    <Fragment>
      <div className='user-container-info-box'>
        <div className='info-box'>
          <h4 className='heading heading__four'>Username:</h4>
          <h4 className='heading heading__four heading__four--admin-info'>
            {user.username}
          </h4>
        </div>
        <div className='info-box'>
          <h4 className='heading heading__four'>Email:</h4>
          <h4 className='heading heading__four heading__four--admin-info'>
            {user.email}
          </h4>
        </div>

        <div className='info-box'>
          <h4 className='heading heading__four'>Forum role:</h4>
          <h4 className='heading heading__four heading__four--admin-info'>
            {user.roles.includes('Admin') ? 'Admin' : 'Basic'}
          </h4>
        </div>
      </div>
    </Fragment>
  );
}

UserInfo.defaultProps = {
  user: {},
}

export default UserInfo;
