import React, { Fragment } from 'react';
import './user-role.css';
import { connect } from 'react-redux';
import { updateUserRole } from '../../../store/actions/index';

const UserRole = (props) => {
  const { user, updateUserRole, closeBox } = props;

  const updateUserRoleHandler = () => {
    if (user.roles.includes('Admin')) {
      updateUserRole({
        ...user,
        roles: ['Basic'],
      });
    } else {
      updateUserRole({
        ...user,
        roles: ['Admin'],
      });
    }
  };

  const closeRoleBoxHandler = () => closeBox();

  return (
    <Fragment>
      <div className='user-roles-content-box'>
        <h4 className='heading heading__three'>
          Make
          <span className='user-roles-content-name'>
            &nbsp;{user.username}&nbsp;
          </span>
          {user.roles.includes('Admin') ? 'a Basic' : 'an Admin'}.
        </h4>
        <div className='user-roles-content-options'>
          <h3
            className='heading heading__three user-roles-content-btns'
            onClick={updateUserRoleHandler}
          >
            OK
          </h3>
          <span className='user-roles-content-slash'>|</span>
          <h3
            className='heading heading__three user-roles-content-btns'
            onClick={closeRoleBoxHandler}
          >
            CANCEL
          </h3>
        </div>
      </div>
    </Fragment>
  );
};

UserRole.defaultProps = {
  user: {},
  updateUserRole: () => {},
  closeBox: () => {},
};

export default connect(null, { updateUserRole })(UserRole);
