import React, { Fragment } from 'react'
import './user-activity.css';
import MyActivity from '../../../containers/activity/my-activity/my-activity';

const UserActivity = (props) => {

  const {currentUserId} = props;

  return (
    <Fragment>
      <div className='user-activity-content-container'>
        <MyActivity userId={currentUserId} />
      </div>
    </Fragment>
  );
}

UserActivity.defaultProps = {
  currentUserId: '',
}

export default UserActivity;
