import React from 'react';
import './header-container.css';

const HeaderContainer = (props) => {
  return <div className='header-container'>{props.children}</div>;
};

export default HeaderContainer;
