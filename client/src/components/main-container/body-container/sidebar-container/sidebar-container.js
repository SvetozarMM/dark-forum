import React from 'react';
import './sidebar-container.css';
import { useHistory } from 'react-router-dom';

const SidebarContainer = (props) => {
  let history = useHistory();

  return (
    <div className='sidebar-container'>
      {props.children}
      <h1 className='section-name-box'>{history.location.pathname.slice(1)}</h1>
    </div>
  );
};

export default SidebarContainer;
