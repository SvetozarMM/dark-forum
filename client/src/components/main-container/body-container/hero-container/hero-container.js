import React from 'react';
import './hero-container.css';

const HeroContainer = (props) => {
  return <div className='hero-container'>{props.children}</div>;
};

export default HeroContainer;
