import React from 'react';
import SectionAction from '../section-action/section-action';

const ActivityOptions = () => {
  return (
    <div>
      <SectionAction name='Activity' link={'./activity'} />
      <SectionAction name='My Posts' link={'./my-posts'} />
      <SectionAction name='My Comments' link={'./my-comments'} />
    </div>
  );
};

export default ActivityOptions;
