import React, { Fragment, useState, memo, useCallback } from 'react';
import './post.css';
import AuthorBox from '../UI/author-box/author-box';
import { Icon } from '@material-ui/core';
import {
  Lock,
  ThumbUpAlt,
  Create,
  LockOpen,
  Delete,
  ExpandMore,
  ExpandLess,
} from '@material-ui/icons';
import moment from 'moment';
import { connect } from 'react-redux';
import AddComment from '../UI/add-comment/add-comment';
import Comments from '../../containers/comments/comments';
import { addComment } from '../../store/actions/comments';
import * as notificator from '../../utils/notificator/notificator';

const Post = memo((props) => {
  const {
    post,
    userId,
    userName,
    isAdmin,
    update,
    like,
    toggleLock,
    deletePost,
    isUserBanned,
    addComment,
  } = props;

  const [commentBoxloaded, setCommentBoxloaded] = useState(false);

  const [isCreateCommentBoxOpen, setIsCreateCommentBoxOpen] = useState(false);
  const [isInEditMode, setIsInEditMode] = useState(false);

  const [postTitle, setPostTitle] = useState(post.title);
  const [postContent, setPostContent] = useState(post.content);

  const commentBoxCloseHandler = useCallback(() => {
    setCommentBoxloaded(false);
    setIsCreateCommentBoxOpen(false);
  }, []);

  const editorModeHandler = useCallback(() => {
    if (!isUserBanned) {
      commentBoxCloseHandler();
      setIsInEditMode(!isInEditMode);
      setPostTitle(post.title);
      setPostContent(post.content);
    } else {
      notificator.warning(`You are temporary banned!`);
    }
  }, [isUserBanned, commentBoxCloseHandler, isInEditMode, post]);

  const postTitleHandler = useCallback((event) => {
    setPostTitle(event.target.value);
  }, []);

  const postContentHandler = useCallback((event) => {
    setPostContent(event.target.value);
  }, []);

  const commentBoxOpenHandler = useCallback(() => {
    setCommentBoxloaded(true);
  }, []);

  const showCreateCommentBox = useCallback(() => {
    if (!isUserBanned) {
      setCommentBoxloaded(true);
      setIsCreateCommentBoxOpen(true);
    } else {
      notificator.warning(`You are temporary banned!`);
    }
  }, [isUserBanned]);

  const hideCreateCommentBox = useCallback(() => {
    setIsCreateCommentBoxOpen(false);
  }, []);

  const updateHandler = useCallback(() => {
    if (post.title !== postTitle || post.content !== postContent) {
      update({
        ...post,
        title: postTitle,
        content: postContent,
      });
    }
    setIsInEditMode(!isInEditMode);
  }, [post, postTitle, postContent, update, isInEditMode]);

  const likeHandler = useCallback(() => {
    if (!post.votes.includes(userName) && !isUserBanned && !post.isLocked) {
      like(post);
    } else if (isUserBanned) {
      notificator.warning(`You are temporary banned!`);
    }
  }, [post, like, userName, isUserBanned]);

  const lockHandler = useCallback(() => {
    toggleLock(post);
  }, [toggleLock, post]);

  const deleteHandler = useCallback(() => {
    if (!isUserBanned) {
      deletePost(post.id);
    } else {
      notificator.warning(`You are temporary banned!`);
    }
  }, [isUserBanned, deletePost, post.id]);

  // Comments actions
  const addCommentHandler = useCallback(
    (comment) => {
      addComment(comment, post.id);
    },
    [addComment, post.id]
  );

  return (
    <Fragment>
      <div className='post-box'>
        {isInEditMode ? (
          <Fragment>
            <div className='post-box__header'>
              <AuthorBox authorName={post.userCreator.username} size='big' />
              <textarea
                className='title-input'
                value={postTitle}
                onChange={postTitleHandler}
              ></textarea>
            </div>
            <div className='post-box__body'>
              <textarea
                className='content-input'
                value={postContent}
                onChange={postContentHandler}
              ></textarea>
            </div>

            <div className='edit-mode-options'>
              <h3
                className='heading heading__three save-post'
                onClick={updateHandler}
              >
                Save
              </h3>
              <span className='slash'>|</span>
              <h3
                className='heading heading__three cancel-post'
                onClick={editorModeHandler}
              >
                Cancel
              </h3>
            </div>
          </Fragment>
        ) : (
          <Fragment>
            <div className='post-box__header'>
              <AuthorBox authorName={post.userCreator.username} size='big' />
              <h2 className='heading heading__two post-box__header-heading'>
                {postTitle}
              </h2>
              {post.isLocked ? (
                <Icon
                  component={Lock}
                  fontSize='default'
                  className='post-box__action-icon post-box__action-icon--locked'
                />
              ) : null}
            </div>
            <div className='post-box__body'>
              <p className='post-box__body-content'>{postContent}</p>
            </div>
          </Fragment>
        )}

        <div className='post-box__footer'>
          <div className='post-box__date'>
            <p>{moment(post.createdOn).format('DD/MM/YYYY - hh:mm:ss')}</p>
          </div>

          <div className='post-box__action'>
            <div className='like-box'>
              <div className='like-number'>{post.votes?.length}</div>
              <Icon
                component={ThumbUpAlt}
                titleAccess={
                  isUserBanned ? 'You are temporary banned!' : 'Like Post'
                }
                style={
                  isUserBanned
                    ? { cursor: 'not-allowed' }
                    : { cursor: 'pointer' }
                }
                fontSize='large'
                className='post-box__action-icon'
                onClick={likeHandler}
              />
            </div>
            {userId === post.userCreator.id &&
            !post.isLocked &&
            !isInEditMode ? (
              <Icon
                component={Create}
                titleAccess={
                  isUserBanned ? 'You are temporary banned!' : 'Edit Post'
                }
                style={
                  isUserBanned
                    ? { cursor: 'not-allowed' }
                    : { cursor: 'pointer' }
                }
                fontSize='large'
                className='post-box__action-icon'
                onClick={editorModeHandler}
              />
            ) : null}
            {isAdmin && !post.isLocked ? (
              <Icon
                component={LockOpen}
                titleAccess='Lock Post'
                fontSize='large'
                className='post-box__action-icon'
                onClick={lockHandler}
              />
            ) : isAdmin ? (
              <Icon
                component={Lock}
                titleAccess='Unlock Post'
                fontSize='large'
                className='post-box__action-icon'
                onClick={lockHandler}
              />
            ) : null}
            {(userId === post.userCreator.id || isAdmin) && !post.isLocked ? (
              <Icon
                component={Delete}
                titleAccess={
                  isUserBanned ? 'You are temporary banned!' : 'Delete Post'
                }
                style={
                  isUserBanned
                    ? { cursor: 'not-allowed' }
                    : { cursor: 'pointer' }
                }
                fontSize='large'
                className='post-box__action-icon'
                onClick={deleteHandler}
              />
            ) : null}
          </div>
        </div>

        {!post.isLocked && !isInEditMode ? (
          <div className='actions-comment-container'>
            <h4
              className='heading heading-comment-actions add-comment'
              onClick={showCreateCommentBox}
              title={isUserBanned ? 'You are temporary banned!' : 'Add Comment'}
              style={
                isUserBanned ? { cursor: 'not-allowed' } : { cursor: 'pointer' }
              }
            >
              Add Comment &#43;
            </h4>
            <h4 className='heading heading-comment-actions view-comments'>
              {commentBoxloaded ? (
                <Fragment>
                  <div onClick={commentBoxCloseHandler}>
                    Hide comments
                    <Icon
                      className='view-comments-icon'
                      component={ExpandLess}
                      fontSize='small'
                    />
                  </div>
                </Fragment>
              ) : (
                <Fragment>
                  <div onClick={commentBoxOpenHandler}>
                    Show comments
                    <Icon
                      className='view-comments-icon'
                      component={ExpandMore}
                      fontSize='small'
                    />
                  </div>
                </Fragment>
              )}
            </h4>
          </div>
        ) : null}

        {commentBoxloaded ? (
          <div className='post-box__comment-container'>
            <Comments postOwnerId={post.id} />

            {isCreateCommentBoxOpen ? (
              <AddComment
                closeCreateBox={hideCreateCommentBox}
                saveComment={addCommentHandler}
              />
            ) : null}
          </div>
        ) : null}
      </div>
    </Fragment>
  );
});

Post.defaultProps = {
  post: {},
  userId: '',
  userName: '',
  isAdmin: false,
  update: () => {},
  like: () => {},
  toggleLock: () => {},
  deletePost: () => {},
  addComment: () => {},
  isUserBanned: false,
};

export default connect(
  (state) => {
    return {
      userId: state.auth.userId,
      userName: state.auth.userName,
      isAdmin: state.auth.isAdmin,
      isUserBanned: state.auth.userBanstatus.isBanned,
    };
  },
  {
    addComment,
  }
)(Post);
