import React from 'react'
import './section-body.css';

const SectionBody = (props) => {
  return (
    <div className='section-body'>{props.children}</div>
  )
}

export default SectionBody;
