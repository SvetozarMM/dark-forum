import React from 'react';
import './section-heading.css';

const SectionHeading = (props) => {
  return <div className='section-heading'>{props.children}</div>;
};

export default SectionHeading;
