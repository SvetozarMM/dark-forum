import React, { Fragment, useState, useCallback } from 'react';
import './comment.css';
import AuthorBox from '../UI/author-box/author-box';
import { connect } from 'react-redux';
import moment from 'moment';
import { Icon } from '@material-ui/core';
import { ThumbUpAlt, Create, Delete } from '@material-ui/icons';
import * as notificator from '../../utils/notificator/notificator';

const Comment = (props) => {
  const {
    userId,
    userName,
    isAdmin,
    comment,
    update,
    like,
    deleteComment,
    isMyCommentsMode,
    isUserBanned,
  } = props;

  const [commentContent, setCommentContent] = useState(comment.content);
  const [isInEditMode, setIsInEditMode] = useState(false);

  const editorModeHandler = useCallback(() => {
    if (!isUserBanned) {
      setIsInEditMode(!isInEditMode);
      setCommentContent(comment.content);
    } else {
      notificator.warning(`You are temporary banned!`);
    }
  }, [isUserBanned, isInEditMode, comment.content ]);

  const commentContentHandler = useCallback((event) => {
    setCommentContent(event.target.value);
  }, []);

  const updateHandler = useCallback(() => {
    if (comment.content !== commentContent) {
      update({
        ...comment,
        content: commentContent,
      });
    }
    setIsInEditMode(!isInEditMode);
  }, [comment, commentContent, update, isInEditMode]);

  const likeHandler = useCallback(() => {
    if (!comment.votes.includes(userName) && !isUserBanned) {
      like(comment);
    } else if (isUserBanned) {
      notificator.warning(`You are temporary banned!`);
    }
  }, [comment, like, isUserBanned, userName]);

  const deleteHandler = useCallback(() => {
    if (!isUserBanned) {
      deleteComment(comment.id);
    } else {
      notificator.warning(`You are temporary banned!`);
    }
  }, [isUserBanned, deleteComment, comment.id]);

  return (
    <Fragment>
      <div className='comment-box'>
        <div className='comment-box__header'>
          <AuthorBox authorName={comment.userCreator.username} size='small' />
        </div>

        {isInEditMode ? (
          <Fragment>
            <div className='comment-box__body'>
              <textarea
                className='comment-content-input'
                value={commentContent}
                onChange={commentContentHandler}
              ></textarea>
            </div>
            <div className='comment-edit-mode-options'>
              <h3
                className='heading heading__three save-post'
                onClick={updateHandler}
              >
                Save
              </h3>
              <span className='slash'>|</span>
              <h3
                className='heading heading__three cancel-post'
                onClick={editorModeHandler}
              >
                Cancel
              </h3>
            </div>
          </Fragment>
        ) : (
          <div className='comment-box__body'>
            <p className='comment-box__body-content'>{commentContent}</p>
          </div>
        )}

        <div className='comment-box__footer'>
          <div className='comment-box__date'>
            <p>{moment(comment.createdOn).format('DD/MM/YYYY - hh:mm:ss')}</p>
          </div>

          {!isMyCommentsMode && (
            <div className='comment-box__action'>
              <div className='comment-like-box'>
                <div className='comment-like-number'>
                  {comment.votes?.length}
                </div>
                <Icon
                  component={ThumbUpAlt}
                  titleAccess={
                    isUserBanned ? 'You are temporary banned!' : 'Like Comment'
                  }
                  style={
                    isUserBanned
                      ? { cursor: 'not-allowed' }
                      : { cursor: 'pointer' }
                  }
                  fontSize='large'
                  className='comment-box__action-icon'
                  onClick={likeHandler}
                />
              </div>
              {userId === comment.userCreator.id && !isInEditMode ? (
                <Icon
                  component={Create}
                  className='comment-box__action-icon'
                  titleAccess={
                    isUserBanned ? 'You are temporary banned!' : 'Edit Comment'
                  }
                  style={
                    isUserBanned
                      ? { cursor: 'not-allowed' }
                      : { cursor: 'pointer' }
                  }
                  fontSize='large'
                  onClick={editorModeHandler}
                />
              ) : null}
              {userId === comment.userCreator.id || isAdmin ? (
                <Icon
                  component={Delete}
                  className='comment-box__action-icon'
                  titleAccess={
                    isUserBanned
                      ? 'You are temporary banned!'
                      : 'Delete Comment'
                  }
                  style={
                    isUserBanned
                      ? { cursor: 'not-allowed' }
                      : { cursor: 'pointer' }
                  }
                  fontSize='large'
                  onClick={deleteHandler}
                />
              ) : null}
            </div>
          )}
        </div>
      </div>
    </Fragment>
  );
};

Comment.defaultProps = {
  userId: '',
  userName: '',
  isAdmin: false,
  comment: {},
  update: () => {},
  like: () => {},
  deleteComment: () => {},
  isMyCommentsMode: false,
  isUserBanned: false,
};

export default connect((state) => {
  return {
    userId: state.auth.userId,
    isAdmin: state.auth.isAdmin,
    userName: state.auth.userName,
    isUserBanned: state.auth.userBanstatus.isBanned,
  };
})(Comment);
