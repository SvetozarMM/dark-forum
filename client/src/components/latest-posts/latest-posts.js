import React, { Fragment } from 'react';
import './latest-posts.css';
import { connect } from 'react-redux';
import LatestPost from '../latest-post/latest-post';

const LatestPosts = (props) => {
  const { latestPosts } = props;

  const posts = latestPosts?.map((post) => {
    return <LatestPost key={post.id} post={post} />;
  });

  return (
    <Fragment>
      <div className='latest-post-container'>
        <h2 className='heading heading__two latest-post-heading'>
          Latest Posts
        </h2>
        <div className='latest-post-list-body'>{posts}</div>
      </div>
    </Fragment>
  );
};

LatestPosts.defaultProps = {
  latestPosts: [],
};

export default connect((state) => {
  return {
    latestPosts: state.posts.latestPosts,
  };
})(LatestPosts);
