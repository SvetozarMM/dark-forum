import React from 'react';
import SectionAction from '../section-action/section-action';

const ControlsOptions = () => {
  return (
    <div>
      <SectionAction name='Users' link={'./controls'} />
    </div>
  );
};

export default ControlsOptions;
