import React, { Fragment, useCallback } from 'react';
import './latest-post.css';
import AuthorBox from '../UI/author-box/author-box';
import { withRouter } from 'react-router-dom';

const LatestPost = (props) => {
  const { post, history } = props;

  const navigateToPostsHandler = useCallback(() => {
    history.push('/posts');
  }, [history]);

  return (
    <Fragment>
      <div className='latest-post-box' onClick={navigateToPostsHandler}>
        <h4 className='heading heading__four latest-post-text'>{post.title}</h4>
        <h5 className='heading heading__five latest-post-text'>
          {post.content.slice(0, 130).concat('...')}
        </h5>
        <AuthorBox authorName={post.userCreator.username} size='small' />
      </div>
    </Fragment>
  );
};

LatestPost.defaultProps = {
  post: {},
  history: [],
};

export default withRouter(LatestPost);
