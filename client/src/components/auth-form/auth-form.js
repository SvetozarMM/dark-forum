import React, { useState, useCallback } from 'react';
import './auth-form.css';
import ErrorLabel from '../UI/error-label/error-label';
import {
  EMAIL_VALIDATION_PATTERN,
  PASSWORD_VALIDATION_PATTERN,
} from '../../constants/constants';

const AuthForm = (props) => {
  const { authHandlerRegister, authHandlerSignIn } = props;

  const [authInfoNameState, setAuthInfoNameState] = useState('');
  const [authInfoIsValidName, setAuthInfoIsValidName] = useState(true);

  const [authInfoEmailState, setAuthInfoEmailState] = useState('');
  const [authInfoIsValidEmail, setAuthInfoIsValidEmail] = useState(true);

  const [authInfoPassState, setAuthInfoPassState] = useState('');
  const [authInfoIsValidPass, setAuthInfoIsValidPass] = useState(true);

  const [isRegisterState, setIsRegisterState] = useState(false);

  const isBtnDisabledHandler = isRegisterState
    ? !authInfoIsValidPass ||
      authInfoPassState === '' ||
      !authInfoIsValidEmail ||
      authInfoEmailState === '' ||
      !authInfoIsValidName ||
      authInfoNameState === ''
    : !authInfoIsValidPass ||
      authInfoPassState === '' ||
      !authInfoIsValidName ||
      authInfoNameState === '';

  const nameValidation = useCallback((name) => {
    const isValid = name.trim().length < 2 || name.trim() === '' ? false : true;
    setAuthInfoIsValidName(isValid);
  }, []);

  const emailValidation = useCallback((email) => {
    const isValid = EMAIL_VALIDATION_PATTERN.test(email);
    setAuthInfoIsValidEmail(isValid);
  }, []);

  const passwordValidation = useCallback((password) => {
    const isValid =
      password.length >= 6 && PASSWORD_VALIDATION_PATTERN.test(password);
    setAuthInfoIsValidPass(isValid);
  }, []);

  const authInfoNameHandler = useCallback(
    (event) => {
      const currentName = event.target.value;
      setAuthInfoNameState(currentName);
      setTimeout(() => nameValidation(currentName), 350);
    },
    [nameValidation]
  );

  const authInfoEmailHandler = useCallback(
    (event) => {
      const currentEmail = event.target.value;
      setAuthInfoEmailState(currentEmail);
      setTimeout(() => emailValidation(currentEmail), 350);
    },
    [emailValidation]
  );

  const authInfoPassHandler = useCallback(
    (event) => {
      const currentPassword = event.target.value;
      setAuthInfoPassState(currentPassword);
      setTimeout(() => passwordValidation(currentPassword), 350);
    },
    [passwordValidation]
  );

  const registerModeHandler = useCallback((isVisible) => {
    setIsRegisterState(isVisible);
  }, []);

  const handleSubmit = useCallback(
    (event) => {
      const name = authInfoNameState;
      const email = authInfoEmailState;
      const password = authInfoPassState;

      event.preventDefault();
      isRegisterState
        ? authHandlerRegister(name, email, password)
        : authHandlerSignIn(name, password);
    },
    [
      authInfoNameState,
      authInfoEmailState,
      authInfoPassState,
      isRegisterState,
      authHandlerRegister,
      authHandlerSignIn,
    ]
  );

  return (
    <div className='auth-form-container'>
      <form onSubmit={handleSubmit} className='form-box'>
        <div className='input-box'>
          <input
            type='text'
            className='input'
            placeholder='Name'
            value={authInfoNameState}
            onChange={authInfoNameHandler}
          />
          <ErrorLabel
            show={authInfoIsValidName}
            msg='Name must be at least 2 characters!'
          />
        </div>

        {isRegisterState ? (
          <div className='input-box'>
            <input
              type='email'
              className='input'
              placeholder='Email'
              value={authInfoEmailState}
              onChange={authInfoEmailHandler}
            />
            <ErrorLabel show={authInfoIsValidEmail} msg='Email is not valid!' />
          </div>
        ) : null}

        <div className='input-box'>
          <input
            type='password'
            className='input'
            placeholder='Password'
            onChange={authInfoPassHandler}
          />
          <ErrorLabel
            show={authInfoIsValidPass}
            msg='Password must be at least 6 characters, at least one uppercase and lowercase letter,  and one number!'
          />
        </div>

        <button className='btn-submit' disabled={isBtnDisabledHandler}>
          {isRegisterState ? 'Register' : 'Sign In'}
        </button>

        <div className='action-btns'>
          <h2
            style={
              !isRegisterState
                ? { color: 'var(--color-gray-darken)' }
                : { color: 'var(--color-red-dark)' }
            }
            className='switch-action-btn'
            onClick={() => registerModeHandler(true)}
          >
            Register
          </h2>
          <h2 className='switch-action-text'>/</h2>
          <h2
            style={
              isRegisterState
                ? { color: 'var(--color-gray-darken)' }
                : { color: 'var(--color-red-dark)' }
            }
            className='switch-action-btn'
            onClick={() => registerModeHandler(false)}
          >
            Sign In
          </h2>
        </div>
      </form>
    </div>
  );
};

AuthForm.defaultProps = {
  authHandlerRegister: () => {},
  authHandlerSignIn: () => {},
};

export default AuthForm;
