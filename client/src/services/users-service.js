import axios from '../utils/axios/axios-users';

export const getAllUsers = (token) => {
  return axios.get(`/all`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

export const getUserByName = (username, token) => {
  return axios.get(`?username=${username}`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

export const getUserActivity = (userId, token) => {
  return axios.get(`/${userId}/activity`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

export const updateUserBanstatus = (user, token) => {
  return axios.put(`/${user.id}/banstatus`, user.banstatus, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

export const updateUserRoles = (user, token) => {
  return axios.put(`/${user.id}/roles`, user.roles, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

export const deleteUser = (user, token) => {
  return axios.delete(`?username=${user.username}`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};
