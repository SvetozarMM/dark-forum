import axios from '../utils/axios/axios-posts';

export const getAllPosts = (token) => {
  return axios.get(`/all`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

export const getPostByTitle = (title) => {
  return axios.get(`/all?title=${title}`);
};

// export const getPostById = (id) => {
//   return axios.get(`/all?id=${id}`);
// };

export const getAllOwnPosts = (token) => {
  return axios.get('', {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

export const createPost = (post, token) => {
  return axios.post('', post, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

export const updatePost = (post, token) => {
  return axios.put(`/${post.id}`, post, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

export const deletePost = (postId, token) => {
  return axios.delete(`/${postId}`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

export const likePost = (post, token) => {
  return axios.put(`/${post.id}/like`, post, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

export const lockPost = (post, token) => {
  return axios.put(`/${post.id}/lockstatus/1`, post, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

export const unlockPost = (post, token) => {
  return axios.put(`/${post.id}/lockstatus/-1`, post, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

export const getComments = (postId, token) => {
  return axios.get(`/${postId}/comments`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

export const addComment = (comment, postId, token) => {
  return axios.post(`/${postId}/comment`, comment, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};
