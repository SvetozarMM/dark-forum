export const save = (key, value) => {
  localStorage.setItem(key, String(value));
};

export const read = (key) => {
  const value = localStorage.getItem(key);

  return value && value !== 'undefined' ? value : null;
};

export const remove = (key) => {
  localStorage.removeItem(key);
};

export const clear = () => {
  localStorage.clear();
};
