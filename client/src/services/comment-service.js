import axios from '../utils/axios/axios-comments';

export const getAllComments = (token) => {
  return axios.get(`/comments/all`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};
export const getAllOwnComments = (token) => {
  return axios.get(`/comments`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

export const updateComment = (comment, token) => {
  return axios.put(`/comment/${comment.id}`, comment, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

export const likeComment = (comment, token) => {
  return axios.put(`/comment/${comment.id}/like`, comment, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

export const deleteComment = (commentId, token) => {
  return axios.delete(`/comment/${commentId}`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};
