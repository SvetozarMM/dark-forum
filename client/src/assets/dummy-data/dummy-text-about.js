import React from 'react';

const DummyTextAbout = () => {
  return (
    <h3 className='heading heading__three about-section-text'>
      &nbsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit.
      Pellentesque efficitur pulvinar lacus, ut interdum tellus tincidunt nec.
      Mauris at sapien justo. Morbi ultrices tellus in lorem pellentesque, in
      sodales tortor fringilla. Maecenas faucibus pulvinar eros vitae tempor.
      Nunc aliquam nibh velit, non varius magna finibus nec. Fusce a lobortis
      nunc. In volutpat elit quis enim mattis fermentum.
      <br />
      <br />
      &nbsp;Aliquam hendrerit mattis augue eget rutrum. Orci varius natoque
      penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin
      tincidunt neque dolor, non euismod lacus imperdiet sit amet.
      <br />
      <br />
      &nbsp;Donec dictum blandit lobortis. Maecenas auctor ligula dui, a posuere
      nulla blandit non. Maecenas semper dignissim ipsum, non iaculis enim
      laoreet dictum. Suspendisse accumsan lorem laoreet urna vulputate mattis.
      Praesent venenatis lorem vitae ex varius, vel molestie sapien malesuada.
      Praesent accumsan, erat ac pretium blandit, leo metus dignissim ante, at
      bibendum justo urna sit amet massa. Integer eleifend vitae sem sit amet
      dapibus.
      <br />
      <br />
      &nbsp;Donec condimentum luctus purus nec molestie. Nunc mattis magna sed
      leo tristique viverra. Mauris egestas molestie tempus. Sed efficitur lorem
      sodales, pretium enim vel, dictum enim. Sed vitae lorem eu lacus dignissim
      pretium. Praesent interdum vel risus non ornare. Donec mollis est id felis
      ullamcorper, et pretium est placerat. Nunc sem nulla, fermentum non mi a,
      gravida tincidunt lacus. Sed euismod magna at egestas fringilla. Morbi vel
      ipsum ac mauris laoreet vestibulum. Integer tristique dapibus arcu vitae
      fringilla. Praesent lacinia elit at tortor lacinia iaculis. Integer in
      tellus in eros varius laoreet. Donec ac est porta justo accumsan pretium
      ac in tortor.
      <br />
      <br />
      &nbsp;Aliquam sollicitudin, diam non porttitor placerat, eros eros rhoncus
      leo, sed bibendum libero libero id neque.
    </h3>
  );
};

export default DummyTextAbout;
