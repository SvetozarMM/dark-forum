export const API_DOMAIN_NAME = 'http://localhost:3000';
export const ADMIN_ROLE = 'Admin';
    
export const EMAIL_VALIDATION_PATTERN = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
export const PASSWORD_VALIDATION_PATTERN = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])/;