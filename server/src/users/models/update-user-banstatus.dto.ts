import { IsString, Length, IsBoolean, IsOptional } from 'class-validator';

export class UpdateUserBanStatusDTO {
    @IsString()
    @Length(0, 200)
    @IsOptional()
    description?: string;

    @IsBoolean()
    isBanned: boolean;
}
