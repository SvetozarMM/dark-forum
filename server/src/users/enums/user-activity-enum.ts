export enum UserActivitiesType {
  Posted = 'Posted',
  Commented = 'Commented',
  Voted = 'Voted',
  Create = 'Created',
}
