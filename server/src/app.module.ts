import { Module } from '@nestjs/common';
import { PostalsModule } from './postals/postals.module';
import { PostCommentsModule } from './post-comments/post-comments.module';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { DatabaseModule } from './data/database.module';
import { ConfigModule } from '@nestjs/config';
import Joi = require('@hapi/joi');

@Module({
  imports: [
    DatabaseModule,
    PostalsModule,
    PostCommentsModule,
    UsersModule,
    AuthModule,
    ConfigModule.forRoot({
      validationSchema: Joi.object({
        PORT: Joi.number().default(3000),
        DB_TYPE: Joi.string().required(),
        DB_HOST: Joi.string().required(),
        DB_PORT: Joi.number().required(),
        DB_USERNAME: Joi.string().required(),
        DB_PASSWORD: Joi.string().required(),
        DB_DATABASE_NAME: Joi.string().required(),
      }),
    }),
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
