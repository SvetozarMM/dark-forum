import {MigrationInterface, QueryRunner} from "typeorm";

export class Initial1589561702632 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `postals` (`id` varchar(36) NOT NULL, `title` varchar(100) NOT NULL, `content` varchar(255) NOT NULL, `createdOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `isLocked` tinyint NOT NULL DEFAULT 0, `isDeleted` tinyint NOT NULL DEFAULT 0, `userCreatorId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `comments` (`id` varchar(36) NOT NULL, `content` text NOT NULL, `createdOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `isDeleted` tinyint NOT NULL DEFAULT 0, `ownerId` varchar(36) NULL, `userCreatorId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `roles` (`id` int NOT NULL AUTO_INCREMENT, `name` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `banstatus` (`id` int NOT NULL AUTO_INCREMENT, `description` varchar(255) NOT NULL DEFAULT '', `isBanned` tinyint NOT NULL DEFAULT 0, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `user` (`id` varchar(36) NOT NULL, `username` varchar(255) NOT NULL, `password` varchar(255) NOT NULL, `email` varchar(255) NOT NULL, `avatar` varchar(255) NULL, `isDeleted` tinyint NOT NULL DEFAULT 0, `banstatusId` int NULL, UNIQUE INDEX `IDX_78a916df40e02a9deb1c4b75ed` (`username`), UNIQUE INDEX `REL_15de78a2ad5e6dbfe54c777d7c` (`banstatusId`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `activity` (`id` int NOT NULL AUTO_INCREMENT, `userActivity` enum ('Posted', 'Commented', 'Voted', 'Created') NOT NULL, `entity` enum ('User', 'Post', 'Comment') NOT NULL, `entityId` varchar(255) NOT NULL, `createdOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `userId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `postals_votes_user` (`postalsId` varchar(36) NOT NULL, `userId` varchar(36) NOT NULL, INDEX `IDX_0f419923bffd12e2956d5eaed1` (`postalsId`), INDEX `IDX_66be0c95dc7e36204a72777f4b` (`userId`), PRIMARY KEY (`postalsId`, `userId`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `comments_votes_user` (`commentsId` varchar(36) NOT NULL, `userId` varchar(36) NOT NULL, INDEX `IDX_5c237bd3103debb70b605adb9f` (`commentsId`), INDEX `IDX_2427ce3958274b36a38bf376f1` (`userId`), PRIMARY KEY (`commentsId`, `userId`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `user_roles_roles` (`userId` varchar(36) NOT NULL, `rolesId` int NOT NULL, INDEX `IDX_0d0cc409255467b0ac4fe6b169` (`userId`), INDEX `IDX_7521d8491e7c51f885e9f861e0` (`rolesId`), PRIMARY KEY (`userId`, `rolesId`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `user_liked_posts_postals` (`userId` varchar(36) NOT NULL, `postalsId` varchar(36) NOT NULL, INDEX `IDX_5426059d35e510164db6da831e` (`userId`), INDEX `IDX_7fbcad188ed6c73082fb41e5da` (`postalsId`), PRIMARY KEY (`userId`, `postalsId`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `user_liked_comment_comments` (`userId` varchar(36) NOT NULL, `commentsId` varchar(36) NOT NULL, INDEX `IDX_99cb2eb902e32e6c52a900575c` (`userId`), INDEX `IDX_83235cec4bbde2708102b3540a` (`commentsId`), PRIMARY KEY (`userId`, `commentsId`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `user_friends_user` (`userId_1` varchar(36) NOT NULL, `userId_2` varchar(36) NOT NULL, INDEX `IDX_04840fd160b733de706a336013` (`userId_1`), INDEX `IDX_e81f236c989f3fd54836b50a12` (`userId_2`), PRIMARY KEY (`userId_1`, `userId_2`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `postals` ADD CONSTRAINT `FK_0c92700743b9a38c3dfcc6f653a` FOREIGN KEY (`userCreatorId`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `comments` ADD CONSTRAINT `FK_c3e176b501c43e0f48a04f58c0e` FOREIGN KEY (`ownerId`) REFERENCES `postals`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `comments` ADD CONSTRAINT `FK_8ad9668eb36ddf0a31f21b25a76` FOREIGN KEY (`userCreatorId`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `user` ADD CONSTRAINT `FK_15de78a2ad5e6dbfe54c777d7c7` FOREIGN KEY (`banstatusId`) REFERENCES `banstatus`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `activity` ADD CONSTRAINT `FK_3571467bcbe021f66e2bdce96ea` FOREIGN KEY (`userId`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `postals_votes_user` ADD CONSTRAINT `FK_0f419923bffd12e2956d5eaed13` FOREIGN KEY (`postalsId`) REFERENCES `postals`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `postals_votes_user` ADD CONSTRAINT `FK_66be0c95dc7e36204a72777f4bb` FOREIGN KEY (`userId`) REFERENCES `user`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `comments_votes_user` ADD CONSTRAINT `FK_5c237bd3103debb70b605adb9f9` FOREIGN KEY (`commentsId`) REFERENCES `comments`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `comments_votes_user` ADD CONSTRAINT `FK_2427ce3958274b36a38bf376f13` FOREIGN KEY (`userId`) REFERENCES `user`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `user_roles_roles` ADD CONSTRAINT `FK_0d0cc409255467b0ac4fe6b1693` FOREIGN KEY (`userId`) REFERENCES `user`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `user_roles_roles` ADD CONSTRAINT `FK_7521d8491e7c51f885e9f861e02` FOREIGN KEY (`rolesId`) REFERENCES `roles`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `user_liked_posts_postals` ADD CONSTRAINT `FK_5426059d35e510164db6da831e5` FOREIGN KEY (`userId`) REFERENCES `user`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `user_liked_posts_postals` ADD CONSTRAINT `FK_7fbcad188ed6c73082fb41e5da7` FOREIGN KEY (`postalsId`) REFERENCES `postals`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `user_liked_comment_comments` ADD CONSTRAINT `FK_99cb2eb902e32e6c52a900575c0` FOREIGN KEY (`userId`) REFERENCES `user`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `user_liked_comment_comments` ADD CONSTRAINT `FK_83235cec4bbde2708102b3540a5` FOREIGN KEY (`commentsId`) REFERENCES `comments`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `user_friends_user` ADD CONSTRAINT `FK_04840fd160b733de706a3360134` FOREIGN KEY (`userId_1`) REFERENCES `user`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `user_friends_user` ADD CONSTRAINT `FK_e81f236c989f3fd54836b50a12d` FOREIGN KEY (`userId_2`) REFERENCES `user`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `user_friends_user` DROP FOREIGN KEY `FK_e81f236c989f3fd54836b50a12d`");
        await queryRunner.query("ALTER TABLE `user_friends_user` DROP FOREIGN KEY `FK_04840fd160b733de706a3360134`");
        await queryRunner.query("ALTER TABLE `user_liked_comment_comments` DROP FOREIGN KEY `FK_83235cec4bbde2708102b3540a5`");
        await queryRunner.query("ALTER TABLE `user_liked_comment_comments` DROP FOREIGN KEY `FK_99cb2eb902e32e6c52a900575c0`");
        await queryRunner.query("ALTER TABLE `user_liked_posts_postals` DROP FOREIGN KEY `FK_7fbcad188ed6c73082fb41e5da7`");
        await queryRunner.query("ALTER TABLE `user_liked_posts_postals` DROP FOREIGN KEY `FK_5426059d35e510164db6da831e5`");
        await queryRunner.query("ALTER TABLE `user_roles_roles` DROP FOREIGN KEY `FK_7521d8491e7c51f885e9f861e02`");
        await queryRunner.query("ALTER TABLE `user_roles_roles` DROP FOREIGN KEY `FK_0d0cc409255467b0ac4fe6b1693`");
        await queryRunner.query("ALTER TABLE `comments_votes_user` DROP FOREIGN KEY `FK_2427ce3958274b36a38bf376f13`");
        await queryRunner.query("ALTER TABLE `comments_votes_user` DROP FOREIGN KEY `FK_5c237bd3103debb70b605adb9f9`");
        await queryRunner.query("ALTER TABLE `postals_votes_user` DROP FOREIGN KEY `FK_66be0c95dc7e36204a72777f4bb`");
        await queryRunner.query("ALTER TABLE `postals_votes_user` DROP FOREIGN KEY `FK_0f419923bffd12e2956d5eaed13`");
        await queryRunner.query("ALTER TABLE `activity` DROP FOREIGN KEY `FK_3571467bcbe021f66e2bdce96ea`");
        await queryRunner.query("ALTER TABLE `user` DROP FOREIGN KEY `FK_15de78a2ad5e6dbfe54c777d7c7`");
        await queryRunner.query("ALTER TABLE `comments` DROP FOREIGN KEY `FK_8ad9668eb36ddf0a31f21b25a76`");
        await queryRunner.query("ALTER TABLE `comments` DROP FOREIGN KEY `FK_c3e176b501c43e0f48a04f58c0e`");
        await queryRunner.query("ALTER TABLE `postals` DROP FOREIGN KEY `FK_0c92700743b9a38c3dfcc6f653a`");
        await queryRunner.query("DROP INDEX `IDX_e81f236c989f3fd54836b50a12` ON `user_friends_user`");
        await queryRunner.query("DROP INDEX `IDX_04840fd160b733de706a336013` ON `user_friends_user`");
        await queryRunner.query("DROP TABLE `user_friends_user`");
        await queryRunner.query("DROP INDEX `IDX_83235cec4bbde2708102b3540a` ON `user_liked_comment_comments`");
        await queryRunner.query("DROP INDEX `IDX_99cb2eb902e32e6c52a900575c` ON `user_liked_comment_comments`");
        await queryRunner.query("DROP TABLE `user_liked_comment_comments`");
        await queryRunner.query("DROP INDEX `IDX_7fbcad188ed6c73082fb41e5da` ON `user_liked_posts_postals`");
        await queryRunner.query("DROP INDEX `IDX_5426059d35e510164db6da831e` ON `user_liked_posts_postals`");
        await queryRunner.query("DROP TABLE `user_liked_posts_postals`");
        await queryRunner.query("DROP INDEX `IDX_7521d8491e7c51f885e9f861e0` ON `user_roles_roles`");
        await queryRunner.query("DROP INDEX `IDX_0d0cc409255467b0ac4fe6b169` ON `user_roles_roles`");
        await queryRunner.query("DROP TABLE `user_roles_roles`");
        await queryRunner.query("DROP INDEX `IDX_2427ce3958274b36a38bf376f1` ON `comments_votes_user`");
        await queryRunner.query("DROP INDEX `IDX_5c237bd3103debb70b605adb9f` ON `comments_votes_user`");
        await queryRunner.query("DROP TABLE `comments_votes_user`");
        await queryRunner.query("DROP INDEX `IDX_66be0c95dc7e36204a72777f4b` ON `postals_votes_user`");
        await queryRunner.query("DROP INDEX `IDX_0f419923bffd12e2956d5eaed1` ON `postals_votes_user`");
        await queryRunner.query("DROP TABLE `postals_votes_user`");
        await queryRunner.query("DROP TABLE `activity`");
        await queryRunner.query("DROP INDEX `REL_15de78a2ad5e6dbfe54c777d7c` ON `user`");
        await queryRunner.query("DROP INDEX `IDX_78a916df40e02a9deb1c4b75ed` ON `user`");
        await queryRunner.query("DROP TABLE `user`");
        await queryRunner.query("DROP TABLE `banstatus`");
        await queryRunner.query("DROP TABLE `roles`");
        await queryRunner.query("DROP TABLE `comments`");
        await queryRunner.query("DROP TABLE `postals`");
    }

}
