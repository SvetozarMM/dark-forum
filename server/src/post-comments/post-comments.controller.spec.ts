import { PostCommentsController } from './post-comments.controller';
import { PostCommentsService } from './post-comments.service';
import { Test, TestingModule } from '@nestjs/testing';
import { ShowUserDTO } from '../users/models/show-user.dto';
import { CreatePostCommentDTO } from './model/create-post-comment-dto';
import { ReturnPostCommentDTO } from './model/return-post-comment-dto';

describe('TodosController', () => {
  let controller: PostCommentsController;
  let postCommentService: Partial<PostCommentsService>;
  let user: ShowUserDTO;
  let comment: CreatePostCommentDTO;
  let expectedComment: ReturnPostCommentDTO;

  beforeEach(async () => {
    postCommentService = {
      all() { return null; },
      allOwn() { return null; },
      create() { return null; },
      update() { return null; },
      delete() { return null; },
      likeComment() { return null; },
    };

    user = {
      id: 'qwerty12345',
      username: 'TestName',
      email: 'testname@test.com',
      roles: ['Basic', 'Admin'],
      avatar: 'test.jpeg',
      banstatus: { id: 1, isBanned: false, description: ''},
    };

    comment = {
      content: 'Test content',
    };

    expectedComment = {
      id: '123qwerty',
      content: 'Test content',
      createdOn: null,
      isDeleted: false,
    };

    const module: TestingModule = await Test.createTestingModule({
      controllers: [PostCommentsController],
      providers: [
        {
          provide: PostCommentsService,
          useValue: postCommentService,
        },
      ],
    }).compile();

    controller = module.get<PostCommentsController>(PostCommentsController);
  });

  it('should be defined', () => {
    // Arrange & Act & Assert
    expect(controller).toBeDefined();
  });

  describe('allComments()', () => {
    it('should call postCommentService all(), once', async () => {
      // Arrange
      const spy = jest
        .spyOn(postCommentService, 'all');

      // Act
      await controller.allComments();

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);

      jest.clearAllMocks();
    });

    it('should return the result from postCommentService all()', async () => {
      // Arrange
      const spy = jest
        .spyOn(postCommentService, 'all')
        .mockReturnValue(Promise.resolve([expectedComment]));

      // Act
      const response = await controller.allComments();

      // Assert
      expect(response).toEqual([expectedComment]);

      jest.clearAllMocks();
    });
  });

  describe('allOwnComments()', () => {
    it('should call postCommentService allOwn(), once', async () => {
      // Arrange
      const spy = jest.spyOn(postCommentService, 'allOwn');

      // Act
      await controller.allOwnComments(user);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);

      jest.clearAllMocks();
    });

    it('should call postCommentService allOwn() with correct parameters', async () => {
      // Arrange
      const spy = jest.spyOn(postCommentService, 'allOwn');

      // Act
      await controller.allOwnComments(user);

      // Assert
      expect(spy).toHaveBeenCalledWith(user);

      jest.clearAllMocks();
    });

    it('should return the result from postCommentService allOwn()', async () => {
      // Arrange
      const spy = jest
        .spyOn(postCommentService, 'allOwn')
        .mockReturnValue(Promise.resolve([expectedComment]));

      // Act
      const response = await controller.allOwnComments(user);

      // Assert
      expect(response).toEqual([expectedComment]);

      jest.clearAllMocks();
    });
  });

  describe('createComment()', () => {
    it('should call postCommentService create(), once', async () => {
      // Arrange
      const postId: string = '12345qwerty';
      const spy = jest.spyOn(postCommentService, 'create');

      // Act
      await controller.createComment(postId, comment, user);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);

      jest.clearAllMocks();
    });

    it('should call postCommentService create() with correct parameters', async () => {
      // Arrange
      const postId: string = '12345qwerty';
      const spy = jest.spyOn(postCommentService, 'create');

      // Act
      await controller.createComment(postId, comment, user);

      // Assert
      expect(spy).toHaveBeenCalledWith(user, comment, postId);

      jest.clearAllMocks();
    });

    it('should return correct message', async () => {
      // Arrange
      const postId: string = '12345qwerty';
      const expectedResult = { message: `Comment created!` };

      jest.spyOn(postCommentService, 'create');

      // Act
      const response = await controller.createComment(postId, comment, user);

      // Assert
      expect(response).toEqual(expectedResult);
    });
  });

  describe('updateComment()', () => {
    it('should call postCommentService update(), once', async () => {
      // Arrange
      const commentId: string = '12345qwerty';
      const spy = jest.spyOn(postCommentService, 'update');

      // Act
      await controller.updateComment(commentId, comment, user);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);

      jest.clearAllMocks();
    });

    it('should call postCommentService update() with correct parameters', async () => {
      // Arrange
      const commentId: string = '12345qwerty';
      const spy = jest.spyOn(postCommentService, 'update');

      // Act
      await controller.updateComment(commentId, comment, user);

      // Assert
      expect(spy).toHaveBeenCalledWith(user, commentId, comment);

      jest.clearAllMocks();
    });

    it('should return correct message', async () => {
      // Arrange
      const commentId: string = '12345qwerty';
      const expectedResult = { message: `Comment updated!` };

      jest.spyOn(postCommentService, 'update');

      // Act
      const response = await controller.updateComment(commentId, comment, user);

      // Assert
      expect(response).toEqual(expectedResult);
    });
  });

  describe('likeComment()', () => {
    it('should call postCommentService likeComment(), once', async () => {
      // Arrange
      const commentId: string = '12345qwerty';
      const spy = jest.spyOn(postCommentService, 'likeComment');

      // Act
      await controller.likeComment(commentId, user);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);

      jest.clearAllMocks();
    });

    it('should call postCommentService likeComment() with correct parameters', async () => {
      // Arrange
      const commentId: string = '12345qwerty';
      const spy = jest.spyOn(postCommentService, 'likeComment');

      // Act
      await controller.likeComment(commentId, user);

      // Assert
      expect(spy).toHaveBeenCalledWith(user.id, commentId);

      jest.clearAllMocks();
    });

    it('should return correct message', async () => {
      // Arrange
      const commentId: string = '12345qwerty';
      const expectedResult = { message: `U've liked it!` };

      jest.spyOn(postCommentService, 'likeComment');

      // Act
      const response = await controller.likeComment(commentId, user);

      // Assert
      expect(response).toEqual(expectedResult);
    });
  });

  describe('deleteComment()', () => {
    it('should call postCommentService delete(), once', async () => {
      // Arrange
      const commentId: string = '12345qwerty';
      const spy = jest.spyOn(postCommentService, 'delete');

      // Act
      await controller.deleteComment(commentId, user);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);

      jest.clearAllMocks();
    });

    it('should call postCommentService delete() with correct parameters', async () => {
      // Arrange
      const commentId: string = '12345qwerty';
      const spy = jest.spyOn(postCommentService, 'delete');

      // Act
      await controller.deleteComment(commentId, user);

      // Assert
      expect(spy).toHaveBeenCalledWith(user, commentId);

      jest.clearAllMocks();
    });

    it('should return correct message', async () => {
      // Arrange
      const commentId: string = '12345qwerty';
      const expectedResult = { message: `Comment deleted!` };

      jest.spyOn(postCommentService, 'delete');

      // Act
      const response = await controller.deleteComment(commentId, user);

      // Assert
      expect(response).toEqual(expectedResult);

      jest.clearAllMocks();
    });
  });

});
