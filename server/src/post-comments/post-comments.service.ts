import { Injectable, BadRequestException } from '@nestjs/common';
import { PostsComments } from '../data/entities/post-comments.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { ReturnPostCommentDTO } from './model/return-post-comment-dto';
import { UserEntity } from '../data/entities/user.entity';
import { Posts } from '../data/entities/postal.entity';
import { CreatePostCommentDTO } from './model/create-post-comment-dto';
import { UpdatePostCommentDTO } from './model/update-post-comment-dto';
import { plainToClass } from 'class-transformer';
import { ForumSystemError } from '../common/exceptions/forum-system.error';
import { ShowUserDTO } from '../users/models/show-user.dto';
import { UserRole } from '../users/enums/user-role.enum';
import { UserActivityService } from '../users/user-activity.service';
import { UserActivitiesType } from '../users/enums/user-activity-enum';

@Injectable()
export class PostCommentsService {

  constructor(
    @InjectRepository(PostsComments) private readonly postsCommentRepository: Repository<PostsComments>,
    @InjectRepository(UserEntity) private readonly usersRepository: Repository<UserEntity>,
    @InjectRepository(Posts) private readonly postsRepository: Repository<Posts>,
    private readonly activityService: UserActivityService,
  ) {}

  async all(withDeleted = false): Promise<ReturnPostCommentDTO[]> {
    const filteredComments: ReturnPostCommentDTO[] = withDeleted
      ? await this.postsCommentRepository.find()
      : await this.postsCommentRepository.find({
        where: { isDeleted: withDeleted },
      });

    return plainToClass(ReturnPostCommentDTO, filteredComments, {
      excludeExtraneousValues: true,
    });
  }

  async postAll(postId: string, withDeleted = false): Promise<ReturnPostCommentDTO[]> {
    const postFound: Posts = await this.postsRepository.findOne({
      where: { id: postId, isDeleted: false },
    });
    if (postFound === undefined) {
      throw new ForumSystemError('Post does not existst!', 400);
    }

    const filteredComments: ReturnPostCommentDTO[] = withDeleted
      ? await this.postsCommentRepository.find({
        where: { owner: postFound },
      })
      : await this.postsCommentRepository.find({
        where: {
          owner: postFound,
          isDeleted: withDeleted,
        },
      });

    return plainToClass(ReturnPostCommentDTO, filteredComments, {
      excludeExtraneousValues: true,
    });
  }

  async allOwn(
    user: ShowUserDTO,
    withDeleted = false,
    ): Promise<ReturnPostCommentDTO[]> {
    const foundUser = await this.usersRepository.findOne({
      where: { username: user.username, isDeleted: false },
    });

    if (foundUser === undefined) {
      throw new ForumSystemError(`User does not exist!`, 404);
    }

    const filteredComments: ReturnPostCommentDTO[] = withDeleted
      ? await this.postsCommentRepository.find({
        where: { userCreator: foundUser },
      })
      : await this.postsCommentRepository.find({
        where: {
          isDeleted: withDeleted,
          userCreator: foundUser,
        },
      });

    return plainToClass(ReturnPostCommentDTO, filteredComments, {
      excludeExtraneousValues: true,
    });
  }

  async create(
    user: ShowUserDTO,
    postComment: CreatePostCommentDTO,
    ownerPostID: string,
    ): Promise<ReturnPostCommentDTO> {
      const userFound: UserEntity = await this.usersRepository.findOne({
        where: { username: user.username, isDeleted: false },
      });
      if (userFound === undefined) {
        throw new ForumSystemError(`User does not exist!`, 404);
      }

      const postFound: Posts = await this.postsRepository.findOne({
        where: { id: ownerPostID, isDeleted: false },
      });
      if (postFound === undefined) {
        throw new ForumSystemError('Post does not existst!', 400);
      }
      if (postFound.isLocked) {
        throw new ForumSystemError(`Locked post!`, 400);
      }

      const newComment: PostsComments = this.postsCommentRepository.create(postComment);

      newComment.owner = Promise.resolve(postFound);
      newComment.userCreator = userFound;
      newComment.votes = [];

      const savedComment = await this.postsCommentRepository.save(newComment);

      await this.activityService.logCommentActivity(userFound, savedComment.id, UserActivitiesType.Commented);

      return plainToClass(ReturnPostCommentDTO, savedComment, {
        excludeExtraneousValues: true,
      });
  }

  async update(
    user: ShowUserDTO,
    commentID: string,
    comment: UpdatePostCommentDTO,
    ): Promise<ReturnPostCommentDTO> {
      const foundComment: PostsComments = await this.findById(commentID);
      const commentCreatorName: string = (await foundComment.userCreator).username;

      const updatedComment = user.roles.includes(UserRole.Admin) || commentCreatorName === user.username
        ? await this.postsCommentRepository.save({...foundComment, content: comment.content})
        : undefined;

      if (updatedComment === undefined) {
        throw new ForumSystemError(`You must be 'Administrator' to change this comment!`, 400);
      }

      return plainToClass(ReturnPostCommentDTO, updatedComment, {
        excludeExtraneousValues: true,
      });
  }

  async delete(
    user: ShowUserDTO,
    commentID: string,
    ): Promise<ReturnPostCommentDTO> {
    const foundComment: PostsComments = await this.findById(commentID);
    const commentCreatorName: string = (await foundComment.userCreator).username;

    const savedComment = user.roles.includes(UserRole.Admin) || commentCreatorName === user.username
      ? await this.postsCommentRepository.save({...foundComment, isDeleted: true})
      : undefined;

    if (savedComment === undefined) {
      throw new ForumSystemError(`You must be 'Administrator' to delete this comment!`, 400);
    }

    return plainToClass(ReturnPostCommentDTO, savedComment, {
      excludeExtraneousValues: true,
    });
  }

  async likeComment(userID: string, commentId: string): Promise<ReturnPostCommentDTO> {
    const comment: PostsComments = await this.findById(commentId);
    const user: UserEntity = await this.usersRepository.findOne({
      where: { id: userID, isDeleted: false },
    });
    if (user === undefined) {
      throw new ForumSystemError('Can not find user', 404);
    }

    const currentVotes = comment.votes;
    comment.votes = [...currentVotes, user];

    const savedComment = await this.postsCommentRepository.save(comment);

    return plainToClass(ReturnPostCommentDTO, savedComment, {
      excludeExtraneousValues: true,
    });
  }

  private async findById(id: string): Promise<PostsComments> {
    const commentFound = await this.postsCommentRepository.findOne({
      where: { id, isDeleted: false },
    });

    if (commentFound === undefined) {
      throw new ForumSystemError(`Comment with id ${id} does not exist!`, 400);
    }

    return commentFound;
  }
}
