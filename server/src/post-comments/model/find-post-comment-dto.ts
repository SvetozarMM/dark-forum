import { IsOptional } from 'class-validator';

export class FindPostCommentDTO {
    @IsOptional()
    id?: string;
}
