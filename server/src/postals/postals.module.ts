import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Posts } from '../data/entities/postal.entity';
import { PostalsController } from './postals.controller';
import { PostalsService } from './postals.service';
import { UserEntity } from '../data/entities/user.entity';
import { UserActivityService } from '../users/user-activity.service';
import { UsersModule } from '../users/users.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Posts, UserEntity]),
    UsersModule,
],
  controllers: [PostalsController],
  providers: [PostalsService],
})
export class PostalsModule {}
