import { Expose, Transform } from 'class-transformer';
import { UserEntity } from '../../../data/entities/user.entity';

export class ReturnPostDTO {
  @Expose()
  id: string;

  @Expose()
  title: string;

  @Expose()
  content: string;

  @Expose()
  createdOn: Date;

  @Expose()
  isLocked: boolean;

  @Expose()
  @Transform((_, obj) => ({
    id: obj.userCreator.id,
    username: obj.userCreator.username,
    avatar: obj.userCreator.avatar,
  }))
  userCreator: UserEntity;

  @Expose()
  @Transform((_, obj) => obj.votes.map((x: any) => x.username))
  votes: UserEntity[];

  isDeleted: boolean;
}
