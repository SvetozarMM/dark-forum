import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder } from '@nestjs/swagger';
import { ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ForumSystemErrorFilter } from './common/filters/forum-error.filter';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const options = new DocumentBuilder()
    .setTitle('Forum API')
    .setDescription('Amazing forum')
    .setVersion('1.0')
    .addTag('forum')
    .build();

  app.useGlobalPipes(new ValidationPipe({ whitelist: true, transform: true }));
  app.useGlobalFilters(new ForumSystemErrorFilter());

  app.enableCors();

  await app.listen(app.get(ConfigService).get('PORT'));
}
bootstrap();
